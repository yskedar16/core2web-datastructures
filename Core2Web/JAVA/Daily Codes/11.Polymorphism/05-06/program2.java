class Parent{
        Parent(){
                System.out.println("Parent Constructor");

        }
	void gun(){
		System.out.println("In Parent Gun");
	}
      	

}
class Child extends Parent {
        Child(){
                System.out.println("In Child Constructor");
        }
        void run(){
                System.out.println("In Child run");

        }

}
class Client {
        public static void main(String[] args){
                Child obj1 = new Child();
                obj1.run();
                obj1.gun();

		Parent obj2 = new Parent();
		obj2.run();
		obj2.gun();

        }

}
