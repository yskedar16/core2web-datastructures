class Parent {
	void fun() {
		System.out.println("Parent Fun");
	}

}
class Child extends Parent{
	private void fun(){
		System.out.println("Child Fun");
	}
}
