class Parent {
	public void fun(){
		System.out.println("Parent Fun");
	}

}
class Child extends Parent {
	void fun() {
		System.out.println("In child Fun");

	}

}
class Client {
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();
	}
}
