class Parent {
	private void fun() {
		System.out.println("Parent Fun");

	}

}
class Child extends Parent {
	void fun(){

		System.out.println("Child Fun");
	}

}
class Client {
	public static void main(String[] args){
		Child obj = new Child();
		obj.fun();	

	}

}
