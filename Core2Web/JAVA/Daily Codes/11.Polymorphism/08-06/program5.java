class Parent {
	static void fun(){
		System.out.println("In fun");
	}

}
class Child extends Parent {
	void fun(){
		System.out.println("In Child");
	}

}
class Client {
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();
	}


}
