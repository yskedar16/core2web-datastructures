class Parent{
	void match(){
		System.out.println("In Parent");
	}


}
class Child extends Parent{
	void fun(double a){
		System.out.println("In Double");
		
	}

}
class Client {
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun(20.5);
	}
}
