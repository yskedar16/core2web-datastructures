class Parent {
	Object fun(){
		System.out.println("Parent Fun");
		return new Object();
	}
}
class Child extends Parent{
	String fun(){
		System.out.println("Child Fun");
		return "Shashi";
	}
}
class Client{
	public static void main(String[] args){
		Parent p = new Child();
		p.fun();
	}
}
