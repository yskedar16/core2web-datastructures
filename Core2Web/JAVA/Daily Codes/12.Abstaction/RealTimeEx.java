abstract class function{
	void timing(){
		System.out.println("In Morning");
	}
	abstract void programSequence();

}
class Child extends function{
	void programSequence(){
		System.out.println("Sequence Of Program");
	}
}
class Client{
	public static void main(String[] args){
		Child obj = new Child();
		obj.timing();
		obj.programSequence();
	}

}
