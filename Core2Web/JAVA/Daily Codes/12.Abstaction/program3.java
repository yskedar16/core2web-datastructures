abstract class Parent{
        void career(){
                System.out.println("Doctor");
        }
        abstract void marry();

}
class Child extends Parent {
        void marry(){
                System.out.println("Kriti Sanon");
        }
}
class Client{
        public static void main(String[] args){
                Parent obj = new Parent();
                obj.career();
                obj.marry();
        }
}
