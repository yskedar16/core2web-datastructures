import java.util.Scanner;
class DataOverflowException extends RuntimeException{
	DataOverflowException(String msg) {
		super(msg);
	}

}
class DataUnderflowException extends RuntimeException{
	DataUnderflowException(String msg){
		super(msg);
	}

}

class ArrayDemo {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int arr[] = new int[5];
		System.out.println("Enter The integer Values");
		System.out.println("Note: 0<element>100");

		for(int i=0; i<arr.length ; i++){
			int a = sc.nextInt();
			if(a<0){
				throw new DataUnderflowException ("number is less than 0");
			}
			if(a>100){
				throw new DataOverflowException ("Number is greater than 100");
			}
			arr[i] = a;
		}
	}


}
