class Parent {
        static {
		System.out.println("In parent Static Block");

	}

}
class Child extends Parent {
	static{
		System.out.println("In Child Constructor");

	}

}
class Client {
	public static void main(String[] args){
		Child obj = new Child();

	}

}

