class Parent{
	@Deprecated
	void m1() {
		System.out.println("Inside Parent-m1");
	}

}
class Client {
	public static void main(String[] args) {
		Parent p = new Child();
		p.m1();
	}
}
