class Parent{
	@Override
	void m1() {
		System.out.println("Inside Parent-m1");
	}

}

class Child extends Parent{ 
	void m1(int x) {
		System.out.println("In Child-m1");
	}
}
class Client {
	public static void main(String[] args) {
		Parent p = new Child();
		p.m1();
	}
}
