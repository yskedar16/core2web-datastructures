//Q.Reverse all the number present between the range 

import java.io.*;
class Demo{

        public static void main(String[] args)throws IOException{

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Start");

                int start=Integer.parseInt(br.readLine());
                System.out.println("Enter end");
                int end=Integer.parseInt(br.readLine());


                for (int i=start;i<=end;i++){

                        int num=i;
                        int rev=0;
                        while(num!=0){


                                int ans=num%10;
                                rev=(rev*10)+ans;
                                num=num/10;

                        }
                        System.out.println(rev);
                }

        }
}
Enter Start
100
Enter end
150
1
101
201
301
401
501
601
701
801
901
11
111
211
311
411
511
611
711
811
911
21
121
221
321
421
521
621
721
821
921
31
131
231
331
431
531
631
731
831
931
41
141
241
341
441
541
641
741
841
941
51

