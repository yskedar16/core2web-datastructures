class Outer {
	static class Inner {
		void m1() {
			System.out.println("In m1-Inner");
		}

	}
	void m1() {
		System.out.println("In m1-outer");
	}

	public static void main(String[] args){
		Inner obj = new Inner();

		obj.m1();
	}
}
