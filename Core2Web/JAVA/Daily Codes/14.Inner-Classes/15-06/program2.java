class Outer {
        class Inner {
                void M1(){
                        System.out.println("Inner-M1");

                }
        }
        void M2() {
                System.out.println("Outer-M2");

        }
}
class Client {
	public static void main(String[] args){
		Outer.Inner obj = new Outer().new Inner();
		obj.M1();
	}
	
}
