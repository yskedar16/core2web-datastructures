ERROR


Code :--
class Demo {

        static int x = 10;
        int y = 20;

        public static void main(String[] args) {
                System.out.println(x);
                System.out.println(y); // Here error comes beacause y is not a Static Variable

        }

}

Output:--

program1.java:8: error: non-static variable y cannot be referenced from a static context
                System.out.println(y);
                                   ^
1 error