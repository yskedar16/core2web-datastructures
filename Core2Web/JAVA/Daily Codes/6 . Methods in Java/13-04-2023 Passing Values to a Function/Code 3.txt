ERROR

Code :--

class Demo{
        void fun(int x){
                System.out.println(x);

        }

        public static void main(String[] args){
                System.out.println("In Main");

                Demo obj = new Demo();
                obj.fun();

                System.out.println("End Main");


        }

}

Output:--

program1.java:11: error: method fun in class Demo cannot be applied to given types;
                obj.fun();
                   ^
  required: int
  found: no arguments
  reason: actual and formal argument lists differ in length
1 error