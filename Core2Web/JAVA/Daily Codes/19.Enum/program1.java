enum ProgLang{

	C,CPP,JAVA,PYTHON
}
class EnumDemo {
	public static void main(String[] args) {
		ProgLang lang = ProgLang.JAVA;

		System.out.println(lang);
		System.out.println(lang.ordinal());
		System.out.println(lang.name());
		System.out.println(lang.toString());
	}
}
