import java.util.*;
class NavigableSetDemo {
	public static void main(String[] args) {
		NavigableSet ns = new TreeSet();
		ns.add(10);
		ns.add(20);
		ns.add(30);
		ns.add(40);
		ns.add(50);

		System.out.println(ns.lower(30));
		System.out.println(ns.floor(30));
		System.out.println(ns.higher(30));
		System.out.println(ns.ceiling(30));
		System.out.println(ns.pollFirst());
		System.out.println(ns);
		System.out.println(ns.pollLast());
		System.out.println(ns);
		System.out.println(ns.pollFirst());

	}

}
