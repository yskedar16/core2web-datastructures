import java.util.*;
class IteratorOnMapDemo {
	public static void main(String[] args) {
		HashMap hm = new HashMap();
		hm.put("Kanha",700);
		hm.put("Ashish",800);
		hm.put("Badhe",200);
		hm.put("Rajesh",500);
	
		System.out.println("Values from HashMap");
		System.out.println(hm);
		
		System.out.println("After Converting Values of HashMap into Set....");
		Set s1 = hm.entrySet();
		System.out.println(s1);

		Iterator itr = s1.iterator();
		System.out.println("Prininting Data Using Iterator");
		while(itr.hasNext()) {
			//Map.Entry m1 = (Map.Entry)itr.next();
			//System.out.println(m1.getKey() + " --> " + m1.getValue());
			System.out.println(itr.next());
		}
	
	


	
	}

}
