import java.util.*;
import java.io.*;

class PropertiesDemo {
	public static void main(String[] args) {
		Properties propertyObj = new Properties();
		FileInputStream fileObj = new FileInputStream("friends.properties");

		propertyObj.load(fileObj);

		String name = propertyObj.getProperty("Ashish");
		System.out.println(name);

		propertyObj.setProperty("Shashi","Binecaps");

		FileOutputStream outObj = new FileOutputStream("friends.properties");
		propertyObj.store(outObj,"Updated by Shashi");

	}
}
