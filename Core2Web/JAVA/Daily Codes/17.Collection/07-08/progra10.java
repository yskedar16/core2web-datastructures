import java.util.*;
import java.util.function.Consumer;

class ConsumerDemo implements Consumer<String> {
	public void accept(String t){
		System.out.println(t);
	}

}
class IteratorDemo {
	public static void main(String[] args) {
                ArrayList al = new ArrayList();
                al.add("Kanha");
                al.add("Rahul");
                al.add("Ashish");

                Iterator itr = al.iterator();
		itr.forEachRemaining(new ConsumerDemo());
			
		

        }

}
