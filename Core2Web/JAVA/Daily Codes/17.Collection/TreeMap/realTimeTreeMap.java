/*
 //Code for Comparable

import java.util.*;

class Seasons implements Comparable {
	String str = null;
	int startingMonth = 0;
	
	Seasons(int startingMonth,String str){
		this.str = str;
		this.startingMonth = startingMonth;
	}
	public String toString() {
		return "{" + str + " : " + startingMonth + "}";
	}
	public int compareTo(Object obj) {
		//return startingMonth - ((Seasons)obj).startingMonth;
		return this.str.compareTo(((Seasons)obj).str);
	}
}

class RealTimeTreeMap {
	public static void main(String[] args) {
		TreeMap tm = new TreeMap();
		tm.put(new Seasons(5,"May-Aug"),"Rainy");
		tm.put(new Seasons(1,"Jan-Apr"),"Summer");
		tm.put(new Seasons(9,"Sep-Dec"),"Winter");
		System.out.println(tm);

	}

}

*/

//Code for Comparator
import java.util.*;
class Seasons{
        String str = null;
        int startingMonth = 0;

        Seasons(int startingMonth,String str){
                this.str = str;
                this.startingMonth = startingMonth;
        }
        public String toString() {
                return "{" + str + " : " + startingMonth + "}";
        }
}
class SortByMonth implements Comparator{
	public int compare(Object obj1,Object obj2) {
		return (((Seasons)obj1).str).compareTo(((Seasons)obj2).str);
	}

}
class SortByStartingMonth implements Comparator{
	public int compare(Object obj1,Object obj2) {
		return ((Seasons)obj1).startingMonth - (((Seasons)obj2).startingMonth);

	}
}
class RealTimeTreeMap {
	public static void main(String[] args) {
		TreeMap tm = new TreeMap(new SortByMonth());
		System.out.println("After Sorting By MonthNames");
		tm.put(new Seasons(5,"May-Aug"),"Rainy");
                tm.put(new Seasons(1,"Jan-Apr"),"Summer");
                tm.put(new Seasons(9,"Sep-Dec"),"Winter");
                System.out.println(tm);

		System.out.println("After Sorting By StartingMonth");
		TreeMap tm2 = new TreeMap(new SortByStartingMonth());
                tm2.put(new Seasons(5,"May-Aug"),"Rainy");
                tm2.put(new Seasons(1,"Jan-Apr"),"Summer");
                tm2.put(new Seasons(9,"Sep-Dec"),"Winter");
                System.out.println(tm2);
	}
	

}
