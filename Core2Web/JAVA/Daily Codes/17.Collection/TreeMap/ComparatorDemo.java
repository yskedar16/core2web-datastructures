import java.util.*;
class Platform {
	String str = null;
	int FoundYear = 0;

	Platform(String str,int FoundYear){
		this.str = str;
		this.FoundYear = FoundYear;
	}
	public String toString() {
		return "{" + str + ":" + FoundYear + "}";
	}		
}
class SortByName implements Comparator {
	public int compare(Object obj1,Object obj2){
		return ((Platform)obj1).str.compareTo(((Platform)obj2).str);
	}
}
class SortByFoundYear implements Comparator {
	public int compare(Object obj1,Object obj2) {
		return ((Platform)obj1).FoundYear - ((Platform)obj2).FoundYear;
	}

}
class TreeMapDemo {
	public static void main(String[] args) {
		TreeMap tm = new TreeMap(new SortByName());
		TreeMap tm2 = new TreeMap(new SortByFoundYear());
				
		tm.put(new Platform("Youtube",2005),"Google");
		tm.put(new Platform("Instagram",2010),"Meta");
		tm.put(new Platform("FaceBook",2004),"Meta");
		tm.put(new Platform("ChatGPT",2023),"OpenAI");

		tm2.put(new Platform("Youtube",2005),"Google");
                tm2.put(new Platform("Instagram",2010),"Meta");
                tm2.put(new Platform("FaceBook",2004),"Meta");
                tm2.put(new Platform("ChatGPT",2023),"OpenAI");

		System.out.println(tm);
		System.out.println(tm2);
	}

}
