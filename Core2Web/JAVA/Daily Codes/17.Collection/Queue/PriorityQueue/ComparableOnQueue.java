import java.util.*;

class Project implements Comparable {
	String proName = null;
	int NoOfDays = 0;
	int duration = 0;
	Project(String proName,int NoOfDays,int duration) {
		this.proName = proName;
		this.NoOfDays = NoOfDays;
	       	this.duration = duration;	
	}
	public String toString(){
		return "{" + proName + " : " + NoOfDays + " : " + duration + "}";
	}
	public int compareTo(Object obj) {
		return this.proName.compareTo(((Project)obj).proName);

	}
}
class ComparableOnQueue {
	public static void main(String[] args) {
		PriorityQueue que = new PriorityQueue();
		que.offer(new Project("Java",80,90));
		que.offer(new Project("ReactJS",20,76));
		que.offer(new Project("NextJs",50,34));

		System.out.println(que);

	}
}




















class ComparableOnDemo {
	

}
