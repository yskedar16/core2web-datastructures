import java.util.*;

class Project {
	String proName = null;
	int NoOfDays = 0;
	int duration = 0;
	Project(String proName,int NoOfDays,int duration) {
		this.proName = proName;
		this.NoOfDays = NoOfDays;
	       	this.duration = duration;	
	}
	public String toString(){
		return "{" + proName + " : " + NoOfDays + " : " + duration + "}";
	}
	
}
class SortByDuration implements Comparator {
	public int compare(Object obj1,Object obj2) {
		return (((Project)obj1).duration) - ((Project)obj2).duration;
	}
}
class ComparatorOnQueue {
	public static void main(String[] args) {
		Queue que = new PriorityQueue(new SortByDuration());
		que.offer(new Project("Java",80,90));
		que.offer(new Project("ReactJS",20,76));
		que.offer(new Project("NextJs",50,34));

		System.out.println(que);

	}
}


