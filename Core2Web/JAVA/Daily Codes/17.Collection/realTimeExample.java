import java.util.*;
class Countries {
	String countryName = null;
	double population = 0.0;
	float gdp = 0.0f;

	Countries(String countryName, double population , float gdp ) {
		this.countryName = countryName;
		this.population = population;
		this.gdp = gdp;
	}
	public String toString() {
		return "{ Country Name-> " + countryName + " : " + "population-> " + population + " : "  + "gdp-> " + gdp + "}";
	}
}
class SortByCountryPopulation implements Comparator {
	public int compare(Object obj1 , Object obj2) {
		return (int)((((Countries)obj1).population) - (((Countries)obj2).population));
	}

}
class SortByCountryName implements Comparator {

	public int compare(Object obj1,Object obj2){
		return (((Countries)obj1).countryName).compareTo(((Countries)obj2).countryName);
	}

}

class Peoples {
	public static void main(String[] args) {
		ArrayList al = new ArrayList();

		al.add(new Countries("India",1425.8,3.14f));
		al.add(new Countries("China", 1425.7,17.73f));
		al.add(new Countries("United_States",333.33,23.32f));
		al.add(new Countries("Indonesia",235.5,23.32f));

		System.out.println(al);

		Collections.sort(al, new SortByCountryPopulation());
		System.out.println(al);
		
		Collections.sort(al, new SortByCountryName());
		System.out.println(al);
	}

}
