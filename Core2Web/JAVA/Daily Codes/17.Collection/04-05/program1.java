import java.util.*;
class ArrayListDemo extends ArrayList {
	public static void main(String[] args) {
		ArrayListDemo al = new ArrayListDemo();
		al.add(10);
		al.add(20.5f);
		al.add("Yogesh");
		al.add(10);
		al.add(20.5f);
		System.out.println(al);

		//2) add(int,E)
		al.add(3,"Core2Web");
		System.out.println(al);

		//3)public int size() 
		System.out.println(al.size());

		//4) public boolean contains(java.lang.object)
		System.out.println(al.contains("Yogesh"));
		System.out.println(al.contains(30));

		//5)public int indexOf(java.lang.object)
		System.out.println(al.indexOf(20.5f));

		//6)public int lastIndexOf(java.lang.Object)
		System.out.println(al.lastIndexOf(20.5f));

		//7)public E get(int)
		System.out.println(al.get(3));

		//8)public E set(int E)
		System.out.println(al.set(3,"Incubater"));

		//9)public boolean addAll(collection)
		ArrayList al2 = new ArrayList();
		al2.add("Salman");
		al2.add("Shahrukh");
		al2.add("Amir");

		al.addAll(al2);
		System.out.println(al);

		//10)public boolean addAll(int,collection)
		al.addAll(3,al2);
		System.out.println(al);

		//11)protected void removeRange(int,int)
		al.removeRange(3,5);
		System.out.println(al);

		//12)public boolean remove(java.lang.Object)
		System.out.println(al.remove(3));
		System.out.println(al);

		//13)public java.lang.object[].toArray()
		Object arr[] = al.toArray();
		for(Object data : arr) {
			System.out.print(data + " ");
		}
		System.out.println();
		
		//14)public void clear()
		al.clear();
		System.out.println(al);




	}

}
