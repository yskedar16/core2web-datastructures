import java.util.*;
class CursorDemo {
	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		al.add("Rahul");
		al.add("Kanha");
		al.add("Ashish");
		al.add("Badhe");

		Iterator cursor = al.iterator();
		while(cursor.hasNext()){
			if("Kanha".equals(cursor.next())){
				cursor.remove();
			}
		}
		System.out.println(al);
	}

}
