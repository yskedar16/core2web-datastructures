import java.util.*;
class CursorDemo {
	public static void main(String[] args ) {
		Vector v = new Vector();
		v.add("Rahul");
		v.add("Kanha");
		v.add("Ashish");
		v.add("Badhe");

		Enumeration cursor = v.elements();
		System.out.println(cursor.getClass().getName());

		while(cursor.hasMoreElements()) {
			System.out.println(cursor.nextElement());
		}
	}

}
