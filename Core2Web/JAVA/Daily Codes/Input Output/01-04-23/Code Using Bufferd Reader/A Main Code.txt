Code:---

import java.io.*;

class Demo {

        public static void main(String[] args) throws IOException {
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);

                System.out.println("Enter your Name");
                String name = br.readLine();


                System.out.println("Enter Your age");
                int age = Integer.parseInt(br.readLine());

                System.out.println("Your name and age is");
                System.out.println(name);
                System.out.println(age);


        }

}



Output:---

Enter your Name
Yogesh
Enter Your age
21
Your name and age is
Yogesh
21