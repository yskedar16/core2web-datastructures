Code:---
import java.io.*;

class Demo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Any String");
                String str1 = br.readLine();

                br.close();

                System.out.println("Enter Second String");
                String str2 = br2.readLine();
                System.out.println(str2);


        }

}


Output:---

Enter Any String
Abc
Enter Second String
Exception in thread "main" java.io.IOException: Stream closed
        at java.base/java.io.BufferedInputStream.getBufIfOpen(BufferedInputStream.java:176)
        at java.base/java.io.BufferedInputStream.read(BufferedInputStream.java:342)
        at java.base/sun.nio.cs.StreamDecoder.readBytes(StreamDecoder.java:284)
        at java.base/sun.nio.cs.StreamDecoder.implRead(StreamDecoder.java:326)
        at java.base/sun.nio.cs.StreamDecoder.read(StreamDecoder.java:178)
        at java.base/java.io.InputStreamReader.read(InputStreamReader.java:181)
        at java.base/java.io.BufferedReader.fill(BufferedReader.java:161)
        at java.base/java.io.BufferedReader.readLine(BufferedReader.java:326)
        at java.base/java.io.BufferedReader.readLine(BufferedReader.java:392)
        at Demo.main(program1.java:16)