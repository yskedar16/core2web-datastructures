class Demo{
	int x = 10;
	Demo(){

		System.out.println("Inside No args Constructor");
	}
	Demo(int x){
		this();
		System.out.println("Inside Parameterised Constructor");
	}

	public static void main(String[] args){
		Demo obj = new Demo(50);
	}

}
