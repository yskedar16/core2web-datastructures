class Demo{
	int x = 10;
	Demo(){
		System.out.println("Inside No parameterised Constructor");
		System.out.println(x);
		System.out.println(this.x);

	}
	Demo(int x){
		System.out.println("Inside parameterised constructor");
		System.out.println(x);
		System.out.println(this.x);

	}
	public static void main(String[] args){
		Demo obj = new Demo();
		Demo obj2 = new Demo(10);

	}


}
