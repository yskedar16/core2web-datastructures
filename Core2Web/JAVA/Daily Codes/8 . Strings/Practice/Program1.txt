Code:--
import java.util.*;
class ReverseDemo{
        StringBuffer reverseDemo(StringBuffer sb){

                String sb1 = sb.toString();
                char ch[] = sb1.toCharArray();
                char ch1[] = new char[sb1.length()];
                int count = 0;
                for(int i=sb1.length()-1 ;i>=0; i-- ){
                        ch1[count] = ch[i];
                        count++;

                }
                System.out.println("Reversed String Is");
                StringBuffer sb2 = new StringBuffer(new String(ch1));
                return sb2;


        }

        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Your String");

                StringBuffer str = new StringBuffer(sc.nextLine());

                ReverseDemo obj = new ReverseDemo();

                StringBuffer str1 = obj.reverseDemo(str);
                System.out.println(str1);



        }

}



Output:---
Enter Your String
Core2Web
Reversed String Is
beW2eroC
