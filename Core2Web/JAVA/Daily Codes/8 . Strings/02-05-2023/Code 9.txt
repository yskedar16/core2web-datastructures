Code :--
class Demo{
        public static void main(String[] args){
                String str1 = "Yogesh";
                String str2 = "Kedar";

                System.out.println(str1);
                System.out.println(str2);

                System.out.println();

                str1 = str1.concat(str2);

                System.out.println(str1);
                System.out.println(str2);

        }

}



Output:---
Yogesh
Kedar

YogeshKedar
Kedar