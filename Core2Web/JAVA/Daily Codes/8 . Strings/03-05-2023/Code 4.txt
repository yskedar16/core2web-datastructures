Code:---
class StringDemo{
        public static void main(String[] args){
                String str1 = "Yogesh";
                String str2 = new String("Yogesh");
                String str3 = "Yogesh";
                String str4 = new String("Yogesh");

                System.out.println(str1.hashCode());
                System.out.println(str2.hashCode());
                System.out.println(str3.hashCode());
                System.out.println(str4.hashCode());


        }


}



Output:---
-1641292823
-1641292823
-1641292823
-1641292823
