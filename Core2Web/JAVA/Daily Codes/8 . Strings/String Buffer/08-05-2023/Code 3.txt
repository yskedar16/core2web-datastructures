Code:---
class StringBufferDemo{
        public static void main(String[] args){
                StringBuffer sb = new StringBuffer();
                System.out.println(sb.capacity());
                System.out.println(sb);

                sb.append("Yogesh");
                System.out.println(sb.capacity());
                System.out.println(sb);

                sb.append("Saudagar");
                System.out.println(sb.capacity());
                System.out.println(sb);


        }


}



Output:---
16

16
Yogesh
16
YogeshSaudagar