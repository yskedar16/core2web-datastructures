Q. WAP to find a palindrome number from an array and return its index.
Take size and elements from the user

Code :--
import java.io.*;
class Demo {
        static int palindrome(int num){
                int num1 = num;
                int num2 = num;
                int ans=0;
                int fResult = 0;
                while(num1 != 0){
                        int ans1 = ans;
                        int rem = num1 % 10;
                        ans = (ans1 * 10) + rem;
                        num1 = num1 / 10;
                }

                return ans;


        }
        public static void main(String[] args) throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size of array");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter Elements of array");
                for(int i=0 ; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());


                }


                for(int i=0 ; i<arr.length ; i++ ){
                        int ans = palindrome(arr[i]);
                        if(ans == arr[i]){
                                System.out.println("Palindrome Number is " + arr[i]);
                        }


                }




        }


}

Output :--

Enter Size of array
5
Enter Elements of array
121
2332
45
24
567
Palindrome Number is 121
Palindrome Number is 2332