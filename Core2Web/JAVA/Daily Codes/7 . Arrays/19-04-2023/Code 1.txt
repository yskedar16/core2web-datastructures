Q. Take Input from User and add all the array Elememts.

Code:---

import java.io.*;
class Demo{
        public static void main(String[] args) throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Array Size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                int arraySum = 0;

                System.out.println("Enter Elements of array");
                for(int i=0 ; i<arr.length ; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                        arraySum=arraySum + arr[i];

                }
                System.out.println("Addition of all Array Elements is :" + arraySum);

        }

}

Output :--
Enter Array Size
7
Enter Elements of array
12
23
34
45
56
67
78
Addition of all Array Elements is :315