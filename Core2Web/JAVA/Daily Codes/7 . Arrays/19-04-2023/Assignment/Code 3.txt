Q . Write a program to take array from user and also take integer elememts from user Print product of Odd index Only

Code :--
import java.io.*;
class Demo{
        public static void main(String[] args) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Size of Array");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                int MulOfDigits = 1;

                System.out.println("Enter array Elements: - ");
                for(int i=0 ; i<arr.length ; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                                if(i % 2 !=0){
                                        MulOfDigits = MulOfDigits * arr[i];

                                }


                }
                System.out.println("Multiplication Of Odd index Elements in array is: " + MulOfDigits);
        }

}

Output:---
Enter Size of Array
6
Enter array Elements: -
1
2
3
4
5
6
Multiplication Of Odd index Elements in array is: 48