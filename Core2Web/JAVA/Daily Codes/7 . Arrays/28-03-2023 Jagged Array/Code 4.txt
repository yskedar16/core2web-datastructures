Code :--
import java.io.*;
class Demo{
        public static void main(String[] args) throws IOException{
                BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

                int arr[] [] = new int[3][];

                arr[0] = new int[3];
                arr[1] = new int[2];
                arr[2] = new int[1];

                for(int i=0;i<arr.length ; i++){
                        for(int j=0; j<arr[i].length ; j++){
                                System.out.println("Enter The Element at " + i + " th " + j + " ");
                                arr[i][j] = Integer.parseInt(br.readLine());

                        }


                }

                System.out.println();
                System.out.println();

                for(int i=0 ; i<arr.length ; i++){

                        for(int j=0; j<arr[i].length ; j++){
                                System.out.print( arr[i][j] + " ");

                        }
                        System.out.println();

                }

        }

}

Output:--
Enter The Element at 0 th 0
1
Enter The Element at 0 th 1
2
Enter The Element at 0 th 2
3
Enter The Element at 1 th 0
4
Enter The Element at 1 th 1
5
Enter The Element at 2 th 0
6


1 2 3
4 5
6
