interface Demo{
	static void fun(){
		System.out.println("Inside static fun");
	}
}
class DemoChild implements Demo{
	
}
class Client{
	public static void main(String[] args){
		Demo.fun();
	}
}
