interface Demo{
	static void fun(){
		System.out.println("In Static fun");
	}
}
class DemoChild implements Demo{
	void fun(){
		System.out.println("Inside DemoChild fun");
		Demo.fun();
	}
}
class Client {
	public static void main(String[] args){
		DemoChild obj = new DemoChild();
		obj.fun();
	}

}
