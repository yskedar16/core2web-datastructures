interface Demo {
	static void fun(){
		System.out.println("Inside Static fun");
	}
}
class ChildDemo implements Demo {

}
class Client {
	public static void main(String[] args){
		ChildDemo obj = new ChildDemo();
		obj.fun();
	}
}
