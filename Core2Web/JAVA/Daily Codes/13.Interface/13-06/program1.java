interface Demo{
	void gun();

	default void fun(){
		System.out.println("Inside Demo fun");

	}

}
class ChildDemo implements Demo{
	public void gun(){
		System.out.println("Inside gun");

	}

}
class Client{
	public static void main(String[] args){
		Demo obj = new ChildDemo();
		obj.gun();
		obj.fun();

	}
}
