interface Demo1{
	default void fun(){
		System.out.println("Inside Demo1 fun");
	}
}
interface Demo2{
	default void fun(){
		System.out.println("Inside Demo2 fun");
	}
}
class ChildDemo implements Demo1,Demo2{
		public void fun(){
			System.out.println("Inside class fun");
		}
}
class Client{
	public static void main(String[] args){
		ChildDemo obj1 = new ChildDemo();
		obj1.fun();

		Demo1 obj2 = new ChildDemo();
		obj2.fun();

		Demo2 obj3 = new ChildDemo();
		obj3.fun();
	}
}
