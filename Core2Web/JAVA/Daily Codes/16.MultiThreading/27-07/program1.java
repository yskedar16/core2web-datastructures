class MyThread extends Thread {
	MyThread(ThreadGroup tg , String str) {
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}

}
class ThreadGroupDemo {
	public static void main(String[] args) throws InterruptedException {
		ThreadGroup pThreadGP = new ThreadGroup("India");

		MyThread obj1 = new MyThread(pThreadGP,"Maharahstra");
		MyThread obj2 = new MyThread(pThreadGP,"Goa");
		obj1.start();
		obj2.start();

		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"Pakistan");
		MyThread obj3 = new MyThread(cThreadGP,"Karachi");
		MyThread obj4 = new MyThread(cThreadGP,"Lahore");
		obj3.start();
		obj4.start();

		ThreadGroup cThreadGP2 = new ThreadGroup(pThreadGP,"Bangaladesh");
                MyThread obj5 = new MyThread(cThreadGP2,"Dhaka");
                MyThread obj6 = new MyThread(cThreadGP2,"Mirpur");
                obj4.start();
                obj5.start();

		cThreadGP.interrupt();
		System.out.println(pThreadGP.activeCount());
		System.out.println(pThreadGP.activeGroupCount());
	}

}
