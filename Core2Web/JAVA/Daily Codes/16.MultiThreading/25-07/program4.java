class MyThread extends Thread {
	MyThread(ThreadGroup tg , String str) {
		super(tg,str);
	}
	public void run() {

		System.out.println(Thread.currentThread());

	}
}
class ThreadGroupDemo {
	public static void main(String[] args) {
		ThreadGroup pthreadGP = new ThreadGroup("Core2Web");

		MyThread obj1 = new MyThread(pthreadGP,"C");
		obj1.start();

		MyThread obj2 = new MyThread(pthreadGP,"CPP");
		obj2.start();

		MyThread obj3 = new MyThread(pthreadGP,"Java");
		obj3.start();
	}

}
