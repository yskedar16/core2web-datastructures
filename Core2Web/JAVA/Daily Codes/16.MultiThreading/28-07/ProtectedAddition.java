package Protectedarithfun;

public class ProtectedAddition {
        public int num1 = 0;
        public int num2 = 0;

        public ProtectedAddition(int num1,int num2) {
                this.num1 = num1;
                this.num2 = num2;
        }
        protected int add() {
                return num1 + num2;
        }

}
