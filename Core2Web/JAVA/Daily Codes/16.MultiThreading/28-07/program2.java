import java.util.Scanner;
import Protectedarithfun.ProtectedAddition;

class Client extends ProtectedAddition {
	Client(int a, int b) {

		super(a,b);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter First Number");
		int num1 = sc.nextInt();
		System.out.println("Enter Second Number");
		int num2 = sc.nextInt();
		
		Client obj = new Client(num1,num2);
		
		
		int addition = obj.add();
		System.out.println("Addition of " + num1 + " and " + num2 + " is " + addition );
		
	}

}
