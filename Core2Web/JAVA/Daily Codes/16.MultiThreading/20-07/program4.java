class Demo implements Runnable{
	public void run() {
		System.out.println("In Demo-run");
		System.out.println("Demo "+ Thread.currentThread().getName());
	}

}
class MyThread implements Runnable {
	public void run() {
		System.out.println("In MyThread run");
		System.out.println("MyThread "+ Thread.currentThread().getName());

		Demo obj = new Demo();
		Thread t = new Thread(obj);
		t.start();
	}
	
}
class ThreadDemo {
	public static void main(String[] args) {
		System.out.println("ThreadDemo "+ Thread.currentThread().getName());
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);
		t.start();

	}

}
