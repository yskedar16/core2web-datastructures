Q  Print cbes ffrom Range

Code:--

import java.io.*;
class Demo{

        public static void main(String[] args) throws IOException{
                BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

                System.out.println("Starting point");
                int start = Integer.parseInt(br.readLine());

                System.out.println("Ending point");
                int end = Integer.parseInt(br.readLine());

                for(int i=start ; i<=end ;i++){
                                int cube = i*i*i;
                                if(cube <= end){
                                        System.out.println("Perfect Cube Between "+ start + "and " + end + "is " +cube);

                                }

                }



        }
}

Output:--
Starting point
1
Ending point
100
Perfect Cube Between 1and 100is 1
Perfect Cube Between 1and 100is 8
Perfect Cube Between 1and 100is 27
Perfect Cube Between 1and 100is 64