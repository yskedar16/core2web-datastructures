Q. Write a Program take 7 character as an input ,Print only vowels from the array 

Code:--
import java.io.*;
class Demo{
        public static void main(String[] args) throws IOException{
                BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

                System.out.println("Enter Size of Array Elements");
                int size = Integer.parseInt(br.readLine());

                char arr[] = new char[size];

                System.out.println("Enter Array Elements");
                for (int i=0 ; i<arr.length ; i++){
                        arr[i] = (char)br.read();
                        br.skip(1);


                }
                System.out.println("Your Array Contains Following Vowels");
                for(int i=0 ; i<arr.length ;i++){
                        if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u'){
                                System.out.println(arr[i]);

                        }

                }

        }
}

Output:---
Enter Size of Array Elements
7
Enter Array Elements
a
b
c
o
d
p
e
Your Array Contains Following Vowels
a
o
e