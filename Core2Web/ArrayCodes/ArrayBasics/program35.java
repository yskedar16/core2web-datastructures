import java.util.*;
class program35 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i=0;i<arr.length;i++) {
			arr[i] = sc.nextInt();
		}

		for(int i=0;i<size-1;i++){
         	   for(int j=0;j<size-1-i;j++){
                	if(arr[j] > arr[j+1]) {
                   		int temp = arr[j];
                    		arr[j] = arr[j+1];
                    		arr[j+1] = temp;
               		}	
            	   }
        	}
		System.out.println("Enter No of elements from which you want Multiplication");
		int key = sc.nextInt();
		int sum = 1;
		for(int i=0;i<key;i++){
			sum = sum * arr[i];
		}
		System.out.println("Final Multiplication is " + sum);

	}
}
