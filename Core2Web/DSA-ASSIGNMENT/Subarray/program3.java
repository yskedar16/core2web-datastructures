/*
 3) Largest subarray of 0's and 1's
Given an array of 0s and 1s. Find the length of the largest subarray with
equal number of 0s and 1s.
Example 1:
Input:
N = 4
A[] = {0,1,0,1}
Output: 4
Explanation: The array from index [0...3]
contains equal numbers of 0's and 1's.
Thus maximum length of subarray having
equal number of 0's and 1's is 4.
Example 2:
Input:
N = 5
A[] = {0,0,1,0,0}
Output: 2
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(N).
*/

class LargestSubarrayOfZerosAndOnes {
	public static void main(String[] args) {
		int arr[] = new int[]{0,1,0,1};
		int ZeroCount = 0;
		int OneCount = 0;
		for(int i=0;i<arr.length;i++) {
			if(arr[i] == 0) 
				ZeroCount++;
			else
				OneCount++;

		}
		for(int i=0;i<arr.length;i++) {
			if(ZeroCount > OneCount) {
				System.out.println(OneCount * 2);
				break;
			}
			else {
				System.out.println(ZeroCount * 2);
				break;
			}

		}

	}
}
