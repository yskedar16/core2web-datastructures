class ReverseSubArray {
	public static void main(String[] args) {
		int arr[] = new int[]{1, 2, 3, 4, 5, 6, 7};
		int L = 2;
		int R = 4;
		for(int i=L;i<R;i++) {
			while(L<R){	
				int temp = arr[L];
				arr[L] = arr[R];
				arr[R] = temp;
				L++;
				R--;
			}	
			
		
		}
		System.out.println("After Reversing");
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}
}
