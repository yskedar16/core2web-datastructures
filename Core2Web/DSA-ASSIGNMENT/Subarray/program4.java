/*
 4) Subarray with 0 sum
Given an array of positive and negative numbers. Find if there is a subarray
(of size at-least one) with 0 sum.
Example 1:
Input:
5
4 2 -3 1 6
Output:
Yes
Explanation:
2, -3, 1 is the subarray
with sum 0.
Example 2:
Input:
5
4 2 0 1 6
Output:
Yes
 */

class SubarrayWithZeroSum {
	boolean ZeroSum(int arr[],int N) {
		for(int i=0;i<N;i++) {
			int Count_Sum = 0;
			for(int j=i;j<N;j++) {
				Count_Sum = Count_Sum + arr[j];
				if(Count_Sum == 0) {
					return true;
				}
			}
		}
		return false;
	}	
}
class MainSubarrayWithZeoSum{

	public static void main(String[] args) {
		int arr[] = new int[]{4,2,-3,1,6};
		int N = arr.length;
		SubarrayWithZeroSum obj = new SubarrayWithZeroSum();
		boolean ans = obj.ZeroSum(arr,N);
		System.out.println(ans);

	}
}


