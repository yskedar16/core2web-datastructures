class MaximumProductSubarray {

	public static void main(String[] args) {
		int arr[] = new int[]{6, -3, -10, 0, 2};
		int result = Integer.MIN_VALUE;
		int leftProduct = 1;
		int rightProduct = 1;
		for(int i=0;i<arr.length;i++) {
			leftProduct *= arr[i];
			rightProduct *= arr[arr.length-i-1];
			result = Math.max(result,leftProduct);
			result = Math.max(result,rightProduct);

			if(arr[i] == 0)
				leftProduct = 1;
			if(arr[arr.length-i-1] == 0)
				rightProduct = 1;
		}
		System.out.println(result);
	}
}
