/* 1) Subarray with given sum
Given an unsorted array A of size N that contains only positive integers, find
a continuous sub-array that adds to a given number S and return the left and
right index(1-based indexing) of that subarray.
In case of multiple subarrays, return the subarray indexes which come first
on moving from left to right.
Note:- You have to return an ArrayList consisting of two elements left and
right. In case no such subarray exists, return an array consisting of element
-1.
Example 1:
Input:
N = 5, S = 12
A[] = {1,2,3,7,5}
Output: 2 4
Explanation: The sum of elements
from 2nd position to 4th position
is 12.
*/

/*
class SubarrayWithGivenSum {
	
	public static void main(String[] args) {
		int arr[] = new int[]{1,2,3,7,5};
		int N = arr.length;
		int RequiredSum = 12;
		
		for(int i=1;i<=N;i++) {
			int sum = 0;
			for(int j=i;j<N;j++) {
				sum = sum + arr[j];
				if(sum == RequiredSum) {
					System.out.println(i + " " + j);
					break;
				}
			}
		
		}

	}
}
*/
class SubArrayWithGivenSum {
	public static void main(String[] args) {
		int arr[] = new int[]{1,2,3,7,5};
		int N = arr.length;
		int S = 12;
		int sum = 0;
		for(int i=0;i<N;i++) {
			int x = 1;
			int y = i+1;
			sum = sum + arr[i];
			if(sum < S) {
				sum = sum + arr[y];
				y++;
			}
			if(sum > S) {
				sum = sum - arr[i];
				x++;
			}
			
			if(sum == S) {
				System.out.println(x + " " + y);	
			}
		}
	}
}
