// Write a program to check if a given number is Harshad Number or not.(A Harshad Number is a number that is divisible by the sum if its digits)

class CheckHarshadNumber {
	boolean harshadNum(int x,int y) {
		if(x == 0) {
			return y%10 == 0;
		}
		else {
			return harshadNum(x/10,y) && x%10 != 0;
		}
	}
	public static void main(String[] args) {
		CheckHarshadNumber obj = new CheckHarshadNumber();
		int harshadNo = 200;
		boolean ans = obj.harshadNum(harshadNo,harshadNo);

		if(ans){
			System.out.println("It is a Harshad Number");
		}
		else {
			System.out.println("It is Not a harshad Number");
		}
	}
}
