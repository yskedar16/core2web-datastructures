// Write a program to check whether Given number is a Duck Number or not
// Duck Number is a number which Doesent start with a zero but has at least one digit as zero
class CheckDuckNumber {
	
		boolean duckNumber(int x) {
			if(x == 0) {
				return false;

			}
			int a = x % 10;
			if(a == 0) {
				return true;
			}
			return duckNumber(x/10);

			//return true;
		}
		public static void main(String[] args) {
			CheckDuckNumber obj = new CheckDuckNumber();
			boolean ans = obj.duckNumber(1023); 
			if(ans) {
				System.out.println("It is Duck Number");
			}
			else {
				System.out.println("It is not a Duck Number");
			}


	}
}
