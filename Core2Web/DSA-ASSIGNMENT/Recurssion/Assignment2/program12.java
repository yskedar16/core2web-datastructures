// Write a program to determine whether a given positive integer is a composite Number or Not

class CheckCompositeNumber {

	boolean compositeNum(int x,int y) {
		if(x<=1) {
			return false;
		}
		if(y > x/2) {
			return false;
		}
		if(x%y == 0) {
			return true;
		}
		return compositeNum(x,y+1);
	}
	public static void main(String[] args) {
		int compositeNo = 8;
		CheckCompositeNumber obj = new CheckCompositeNumber();
		boolean ans = obj.compositeNum(compositeNo,2);
		if(ans) {
			System.out.println("It is a Composite Number");
		}
		else {
			System.out.println("It is not a Composite Number");
		}
	}
}
