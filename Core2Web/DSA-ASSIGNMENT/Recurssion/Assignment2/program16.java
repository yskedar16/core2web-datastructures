// Write a program that determines whether a given number is a cube number or not
// A cube Number is defined as a number that is the cube of an integer

class CheckCubeNumber {
	boolean cubeNumber(int x,int i) {
		int cube = i * i * i;
		if(i>x) {
			return false;
		}
		if(cube == x) {
			return true;
		}
		else if(cube > x) {
			return false;
		}
		else {
			return cubeNumber(x,i+1);
		}
	}


	public static void main(String[] args) {
		CheckCubeNumber obj = new CheckCubeNumber();
		boolean ans = obj.cubeNumber(9,1);
		if(ans) {
			System.out.println("It is a Cube Number");
		}
		else {
			System.out.println("It is not a Cube Number");
		}

	}
}
