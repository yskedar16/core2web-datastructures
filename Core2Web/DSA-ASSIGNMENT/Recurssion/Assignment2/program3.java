

class MaximumNumber {
	static int maxi = 0;
	int fun(int n)  {
		if(n == 0) {
			return 1;
		}
		int a = n % 10;
		maxi = Math.max(a,maxi);
		fun(n / 10);
	        return maxi;	
		

		
	}
	public static void main(String[] args) {
		MaximumNumber obj = new MaximumNumber();
		int ans = obj.fun(163);
		System.out.println(ans);

	}
}
