// Write a program to print the sum of the odd digits in the number

class SumOfOddDigits {
	//static int oddSum = 0;
	int fun(int x){
		if(x == 0) {
			return 0;
		}
		int a = x%10;
		int y = 0;
		if(a%2 !=0) {
			y = a;
		}
		return y + fun(x/10);

		
	}

	public static void main(String[] args) {
		SumOfOddDigits obj = new SumOfOddDigits();
		int ans = obj.fun(11111);
		System.out.println(ans);
	}
}
