// Write a program to Check if a given number is abundant Number or Not.
// (An Abundant Number is the sum of all its proper divisors,denoted by sum(n),is grater than the numberrs value)

class CheckAbundantNumber {
	static int sum = 0;
	int abundantNum(int no,int i){
		if(i > no/2){
			return sum;
		}
		if(no%i == 0){
			sum = sum + i;
		}
		abundantNum(no,i+1);
		return sum;
		
	}
	public static void main(String[] args) {
		CheckAbundantNumber obj = new CheckAbundantNumber();
		int no = 12;
		int ans = obj.abundantNum(no,1);
		if(ans > no) {
			System.out.println("It is Abundant Number");
		}
		else {
			System.out.println("It is not an Abundant Number");
		}
	}
}
