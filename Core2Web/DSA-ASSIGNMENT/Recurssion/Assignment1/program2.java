class Numbers10to1 {
	void fun(int x) {
		if(x < 1) {
			return;
		}
		System.out.println(x);
		fun(--x);
		
			
	}

	public static void main(String[] args) {
		Numbers10to1 obj = new Numbers10to1();
		obj.fun(10);
	}

}
