class FactorialOfNumber {
	int fun(int x) {
		if(x == 1) {
			return 1;
		}

		return x * fun(--x);

	}
	public static void main(String[] args) {
		FactorialOfNumber obj = new FactorialOfNumber();
		int fact = obj.fun(3);
		System.out.println(fact);
	}
	
}
