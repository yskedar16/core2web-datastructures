class SumOFDigit {
	int fun(int x) {
		if(x == 0) {
			return 0;
		}
		
		return x%10 + fun(x/10);
	}
	public static void main(String[] args) {
		SumOFDigit obj = new SumOFDigit();
		int sum = obj.fun(123);
		System.out.println(sum);
	}
}
