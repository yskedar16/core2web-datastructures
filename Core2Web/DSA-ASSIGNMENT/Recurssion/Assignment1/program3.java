class Sum1to10 {
	int fun(int x) {
		if(x == 1) {
			return 1;
		}
		
		return x + fun(--x);
		
			
	}

	public static void main(String[] args) {
		Sum1to10 obj = new Sum1to10();
		int sum = obj.fun(10);
		System.out.println(sum);
	}

}
