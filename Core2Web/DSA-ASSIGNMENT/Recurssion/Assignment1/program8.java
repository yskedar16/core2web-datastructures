class OccuranceOfADigit {
	int fun(int x,int target) {
		int count = 0;
		while(x!=0) {
			int a = x % 10;
			if(a == target) {
				count ++;
			}
			x = x /10;
		}
		return count;
	}
	
	int fun2(int x,int target) {
		int a = target;
		if(x == 0){
			return 0;
		}
		int count2 = 0;
		int y = x % 10;
		if( y == target){
			count2 = 1;
		}
		//int y = x % 10;
		x = x /10;
		//fun(y,a);
		return count2 + fun2(x,a); 
	}
	public static void main(String[] args) {
		OccuranceOfADigit obj = new OccuranceOfADigit();
		int occ = obj.fun(1112231,1);
		int occ1 = obj.fun2(1112231,1);

		System.out.println(occ);
		System.out.println(occ1);
	}
}
