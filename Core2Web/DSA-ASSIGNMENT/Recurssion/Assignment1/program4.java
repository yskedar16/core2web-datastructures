class LengthOfDigit {
	int fun(int x) {
		if(x == 0){
			return 0;
		}
		int count = 1;
		
		return count + fun(x/10);
	}
	public static void main(String[] args) {
		LengthOfDigit obj = new LengthOfDigit();
		int len = obj.fun(11223);
		System.out.println(len);
	}
}
