class Numbers1to10 {
	void fun(int x) {
		if(x == 1) {
			return;
		}
		//System.out.println(x);
		fun(--x);
		System.out.println(x);
			
	}

	public static void main(String[] args) {
		Numbers1to10 obj = new Numbers1to10();
		obj.fun(10);
	}

}
