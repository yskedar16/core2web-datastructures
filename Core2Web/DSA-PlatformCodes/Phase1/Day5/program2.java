/*
 Given an array A of n positive numbers. The task is to find the first index in the array such that the sum of elements before it is equal to the sum of elements after it.

Note:  Array is 1-based indexed.

Example 1:

Input: 
n = 5 
A[] = {1,3,5,2,2} 
Output: 3 
Explanation: For second test case 
at position 3 elements before it 
(1+3) = elements after it (2+2). 
 

Example 2:

Input:
n = 1
A[] = {1}
Output: 1
Explanation:
Since its the only element hence
it is the only point.
Your Task:
The task is to complete the function equalSum() which takes the array and n as input parameters and returns the point. Return -1 if no such point exists.

Expected Time Complexily: O(N)
Expected Space Complexily: O(1)

Constraints:
1 <= n <= 106
1 <= A[i] <= 108



*/
class Solution2{
	static int equalSum(int [] A, int N) {
	    int prefixSum[] = new int[N];
	    prefixSum[0] = A[0];
	    if(N == 1) {
	        return 1;
	    }
	    for(int i=1;i<N;i++) {
	        prefixSum[i] = prefixSum[i-1] + A[i];
	    }

	    for(int i=0;i<N;i++){
	        int leftSum;
	        if (i > 0) {
                leftSum = prefixSum[i - 1];
            } else {
                leftSum = 0;
            }

            int rightSum = prefixSum[N - 1] - prefixSum[i];

            if(rightSum == leftSum) {
                return i + 1;
            }
	    }
	    return -1;
	}
	public static void main(String[] args) {
		int arr[] = {1,3,5,2,2};
		int ans = equalSum(arr,5);
		System.out.println(ans);
	}
}
