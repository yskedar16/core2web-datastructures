// 283. Move Zeroes
import java.util.*;
class Solution1 {
    public static int[] moveZeroes(int[] nums) {
         ArrayList<Integer> temp = new ArrayList<>();

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0)
                temp.add(nums[i]);
        }


        int nz = temp.size();


        for (int i = 0; i < nz; i++) {
            nums[i] = temp.get(i);
        }

        for (int i = nz; i < nums.length; i++) {
        	nums[i] = 0;
        }
	return nums;
    }
    public static void main(String[] args) {
	int arr[] = {0,1,0,3,12};
	int arr2[] = moveZeroes(arr);

	for(int i=0;i<arr2.length;i++) {
		System.out.print(arr2[i] + " ");
	}
    }
}
