// Product of maximum in first array and minimum in second

class Solution2 {

    // Function for finding maximum and value pair
    static long find_multiplication (int arr[], int brr[], int n, int m) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        for(int i=0;i<arr.length;i++) {
            if(arr[i] > max) {
                max = arr[i];
            }
        }
        for(int i=0;i<brr.length;i++) {
            if(brr[i] < min) {
                min = brr[i];
            }
        }
        return max * min;
   }
   public static void main(String[] args) {
	int arr1[] = {5, 7, 9, 3, 6, 2};
	int arr2[] = {1, 2, 6, -1, 0, 9};
	long ans = find_multiplication(arr1,arr2,6,6);
	System.out.println(ans);	
   }


}
