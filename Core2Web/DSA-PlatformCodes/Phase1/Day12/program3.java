// Left most and right most index

class pair  {
    long first, second;
    public pair(long first, long second)
    {
        this.first = first;
        this.second = second;
    }
}

class Solution {

    public pair indexes(long v[], long x)
    {
        int firstOccuranceNo = firstOccurance(v,x);
        int lastOccuranceNo = lastOccurance(v,x);



        return new pair(firstOccuranceNo,lastOccuranceNo);

    }
    public int firstOccurance(long v[],long x){
       for(int i=0;i<v.length;i++) {
            if(v[i] == x) {
                return i;

            }
        }
        return -1;
    }
    public int lastOccurance(long v[],long x) {
        int lastOcc = -1;
        for(int i=0;i<v.length;i++){
            if(v[i] == x) {
                lastOcc = i;
            }
        }
        if (lastOcc == -1) {
            return -1;
        } else {
            return lastOcc;
        }
    }

}
