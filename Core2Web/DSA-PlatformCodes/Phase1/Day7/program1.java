
// Check if Array is Sorted and Rotated
class Solution {
    static boolean check(int[] nums) {
        
        boolean rotated = false;

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] < nums[i - 1]) {
                if (rotated) {
                    return false;
                }
                rotated = true;
            }
        }

        if (rotated) {
            return nums[nums.length - 1] <= nums[0];
        }

        return true;
    }

    public static void main(String[] args) {
	int nums[] = {3,4,5,1,2};
	boolean ans = check(nums);
	System.out.println(ans);
    }
}
