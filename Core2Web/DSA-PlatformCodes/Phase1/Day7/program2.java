// Wave Array
class Solution {
    static void convertToWave(int n, int[] arr) {
        for (int i = 1; i < n; i += 2) {
            if (i < n && arr[i] > arr[i - 1]) {
                int temp = arr[i];
                arr[i] = arr[i - 1];
                arr[i - 1] = temp;
            }

            if (i + 1 < n && arr[i] > arr[i + 1]) {
                int temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }
    public static void main(String[] args) {
	int arr[] = {1,2,3,4,5};
	convertToWave(5,arr);

	for(int i=0;i<arr.length;i++) {
		System.out.println(arr[i]);
	}
    }
}
