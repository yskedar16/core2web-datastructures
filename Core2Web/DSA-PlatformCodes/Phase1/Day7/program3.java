// Convert Array Into Zig-Zag Fashion
class Solution{
    static void zigZag(int a[], int n){
        for (int i = 1; i < n; i++) {
            if ((i % 2 == 0 && a[i] > a[i - 1]) || (i % 2 == 1 && a[i] < a[i - 1])) {
                int temp = a[i];
                a[i] = a[i - 1];
                a[i - 1] = temp;
            }
        } 
    }
    public static void main(String[] args) {
	int arr[]= {4,3,7,8,6,2,1};
	zigZag(arr,7);

	for(int i=0;i<arr.length;i++) {
		System.out.print(arr[i] + " , ");
	}
    }
}
