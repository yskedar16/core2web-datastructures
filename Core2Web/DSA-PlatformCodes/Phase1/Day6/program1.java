import java.util.*;
class Solution {
    static int removeDuplicates(int[] nums) {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        for (int i = 0; i < nums.length; i++) {
            set.add(nums[i]);
        }

        int j = 0;
        for (int x : set) {
            nums[j] = x;
            j++;
        }
        return set.size();
    }
    public static void main(String[] args) {
	int nums[] = {0,0,1,1,1,2,2,3,3,4};
	int ans = removeDuplicates(nums);

	System.out.println(ans);
    }
}
