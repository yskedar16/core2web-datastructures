// Chocalte Distribution Problem
import java.util.*;
class Solution {
	static long findMinDiff(ArrayList<Integer> a,int n,int m) {
		if(m==0 || n==0) {
			return 0;
		}
		Collections.sort(a);

		if(n<m)
			return -1;
		int min = Integer.MAX_VALUE;

		for(int i=0;i+m-1<n;i++) {
			int diff = a.get(i+m-1) - a.get(i);

			if(diff<min) {
				min = diff;
			}
		}
		return min;
	}
	public static void main(String[] args) {
		int N = 8;
		int M = 5;
		ArrayList<Integer> a = new ArrayList<>();
		a.add(3);
		a.add(4);
		a.add(1);
		a.add(9);
		a.add(56);
		a.add(7);
		a.add(9);
		a.add(12);
		long ans = findMinDiff(a,N,M);
		System.out.println(ans);
	}

}
