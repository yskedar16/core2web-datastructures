// Product array puzzle

class Solution {
   static long[] productExceptSelf(int nums[], int n) {
        long arr[] = new long[n];
        for(int i = 0; i < n; i++) {
            long ans = 1;
            for(int j = 0; j < n; j++) {
                if (j != i) {
                    ans *= nums[j];
                }
            }
            arr[i] = ans;
        }
        return arr;
    }
    public static void main(String[] args) {
	int nums[] = {10, 3, 5, 6, 2};
	int n = 5;
	long arr2[] = productExceptSelf(nums,5);
	for(int i=0;i<arr2.length;i++) {
		System.out.print(arr2[i] + " ");
	}
    }
}
