// 977. Squares of a Sorted Array
import java.util.*;
class Solution1 {
    public static int[] sortedSquares(int[] nums) {
        int n = nums.length;
        int[] squaredNums = new int[n];

        for (int i = 0; i < n; i++) {
            squaredNums[i] = nums[i] * nums[i];
        }

        Arrays.sort(squaredNums);

        return squaredNums;
    
    }

    public static void main(String[] args) {
	int arr[] = {-4,-1,0,3,10}; 
	int arr2[] = sortedSquares(arr);
	for(int i=0;i<arr2.length;i++) {
		System.out.print(arr2[i] + " ");
	}
    }
}
