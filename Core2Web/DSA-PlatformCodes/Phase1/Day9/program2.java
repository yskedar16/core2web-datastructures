// Apply Operations to an Array
import java.util.*;
class Solution {
    public static int[] applyOperations(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                nums[i] = nums[i] * 2;
                nums[i + 1] = 0;
                i++;
            }
        }

        Stack<Integer> s = new Stack<>();
         int[] arr2 = new int[nums.length];
        int start = 0;
        int end = nums.length - 1;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                arr2[start] = nums[i];
                start++;
            }
        }

        return arr2;
    }

    public static void main(String[] args) {
	int arr[] = {1,2,2,1,1,0};
	int ans[] = applyOperations(arr);
	for(int i=0;i<ans.length;i++) {
		System.out.print( ans[i] + " , ");
	}
    }
}

