class BuildingReceivingSunlight {

	public static void main(String[] args) {
		int arr[] = {6,2,8,4,11,13};

		int receiver = arr[0];
		int count = 1;

		for(int i=1;i<arr.length;i++) {
			if(arr[i] > receiver) {
				count++;
				receiver = arr[i];
			}
		}
		System.out.println(count);
	}

}
