/*Code3: Key Pair
Company: Zoho, Flipkart, Morgan Stanley, Accolite, Amazon, Microsoft,
FactSet, Hike, Adobe, Google, Wipro, SAP Labs, CarWale
Platform: GFG
Description:
Given an array Arr of N positive integers and another number X. Determine
whether or not there exist two elements in Arr whose sum is exactly X.
Example 1:
Input:
N = 6, X = 16
Arr[] = {1, 4, 45, 6, 10, 8}
Output: Yes
Explanation: Arr[3] + Arr[4] = 6 + 10 = 16
Example 2:
Input:
N = 5, X = 10
Arr[] = {1, 2, 4, 3, 6}
Output: Yes
Explanation: Arr[2] + Arr[4] = 4 + 6 = 10

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N ≤ 105
1 ≤ Arr[i] ≤ 105*/

import java.util.HashSet;

class Solution {
    static boolean hasArrayTwoCandidates(int arr[], int n, int sum) {
        HashSet<Integer> set = new HashSet<>();

        for (int i = 0; i < n; i++) {
            int complement = sum - arr[i];

            if (set.contains(complement)) {
                return true;
            }

            set.add(arr[i]);
        }

        return false;
    }

    public static void main(String args[]) {
        int arr1[] = {1, 4, 45, 6, 10, 8};
        int n1 = 6;
        int sum1 = 16;
        System.out.println(hasArrayTwoCandidates(arr1, n1, sum1)); // Output: Yes

        int arr2[] = {1, 2, 4, 3, 6};
        int n2 = 5;
        int sum2 = 10;
        System.out.println(hasArrayTwoCandidates(arr2, n2, sum2)); // Output: Yes
    }
}

