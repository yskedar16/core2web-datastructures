/*Code 1:Single Number
Company : Amazon, wipro, Capgemini, DXC technology, Schlumberger,
Avizva, epam, cadence, paytm, atlassian,cultfit+7
Platform: LeetCode - 136
Striver’s SDE Sheet
Description:
Given a non-empty array of integers nums, every element appears
twice except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use
only constant extra space.
Example 1:
Input: nums = [2,2,1]
Output: 1
Example 2:
Input: nums = [4,1,2,1,2]
Output: 4
Example 3:
Input: nums = [1]
Output: 1
Constraints:
1 <= nums.length <= 3 * 10^4
-3 * 104 <= nums[i] <= 3 * 10^4
Each element in the array appears twice except for one element
which appears only once.*/

import java.util.*;

class SingleNumber{

	public static int SingleNum(int arr[]){
	
		int result = 0;

	        for (int num : arr) {
        		    result ^= num;
        	}

        	return result;
    }
	

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size of Array ");

		int size = sc.nextInt();

		System.out.println("Enter the array Element");
			
		int arr[] = new int[size];

		for(int i=0;i<size;i++){
		
			arr[i] = sc.nextInt();
		}

		int res = SingleNum(arr);

		System.out.println("Non repeted element is : "+ res);
	}
}

