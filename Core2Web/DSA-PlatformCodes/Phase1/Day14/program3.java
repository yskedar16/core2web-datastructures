// Smallest subarray with sum greater than x
class Solution3 {
     static int smallestSubWithSum(int a[], int n, int x) {
        int minLength = Integer.MAX_VALUE;
        int currentSum = 0;
        int start = 0;

        for (int i = 0; i < n; i++) {
            currentSum = currentSum + a[i];

            while (currentSum > x) {
                minLength = Math.min(minLength, i - start + 1);
                currentSum = currentSum - a[start];
                start++;
            }
        }

        if (minLength == Integer.MAX_VALUE) {
            return 0;
        } else {
            return minLength;
       }
    }
    public static void main(String[] args) {
	int arr[] = {1, 4, 45, 6, 0, 19};
	int n = 6;
	int x = 51;
	int ans = smallestSubWithSum(arr,n,x);
	System.out.println(ans);
    }
}
