// Length Unsorted Subarray
//
import java.util.*;
class Solve {
    static int[] printUnsorted(int[] arr, int n) {
        ArrayList<Integer> arrList = new ArrayList<>();
        if (n == 1) {
            return new int[]{0, 0};
        }

        int start;
        for (start = 0; start < n - 1; start++) {
            if (arr[start] > arr[start + 1]) {
                break;
            }
        }

        if (start == n - 1) {
            return new int[]{-1};
        }

        int end;
        for (end = n - 1; end > 0; end--) {
            if (arr[end] < arr[end - 1]) {
                break;
            }
        }

        int minInUnsorted = Integer.MAX_VALUE;
        int maxInUnsorted = Integer.MIN_VALUE;
        for (int i = start; i <= end; i++) {
            minInUnsorted = Math.min(minInUnsorted, arr[i]);
            maxInUnsorted = Math.max(maxInUnsorted, arr[i]);
        }

        while (start > 0 && arr[start - 1] > minInUnsorted) {
            start--;
        }

        while (end < n - 1 && arr[end + 1] < maxInUnsorted) {
            end++;
        }

        arrList.add(start);
        arrList.add(end);

        int[] finalArr = new int[arrList.size()];
        for (int i = 0; i < finalArr.length; i++) {
            finalArr[i] = arrList.get(i);
        }

        return finalArr;
    }
    public static void main(String[] args) {
	int arr[] = new int[]{10,12,20,30,25,40,32,31,35,50,60};

	int ans[] = printUnsorted(arr,11);

	for(int i=0;i<ans.length;i++) {
		System.out.print(ans[i] + " ");
	}
    }
}
