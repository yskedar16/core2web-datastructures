// Intersection of Two Arrays

import java.util.*;
class Solution1 {
    static int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> resultSet = new HashSet<>();

        for (int num : nums1) {
            set1.add(num);
        }

        for (int num : nums2) {
            if (set1.contains(num)) {
                resultSet.add(num);
            }
        }

        int[] resultArray = new int[resultSet.size()];
        int i = 0;
        for (int num : resultSet) {
            resultArray[i++] = num;
        }

        return resultArray;
    }
    public static void main(String[] args) {
	int nums1[] = {1,2,2,1};
	int nums2[] = {2,2};

	int ans[] = intersection(nums1,nums2);
	for(int i=0;i<ans.length;i++) {
		System.out.print(ans[i]);
	}
    }
}
