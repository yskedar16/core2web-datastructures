// Binary Search

class Solution2 {
    static int binarysearch(int arr[], int n, int k) {
        int low = 0;
        int high = n-1;

        while(low <= high) {
            int mid = (low + high) / 2;

            if(arr[mid] == k) {
                return mid;
            }
            else if(mid > 0 && arr[mid] > k) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return -1;
    }
    public static void main(String[] args) {
	int arr[] = {1,2,3,4,5};
	int n = 5; 
	int k = 4;
	int ans = binarysearch(arr,n,k);
	System.out.println(ans);
    }
}
