// Bitonic Point
class Solution3 {
    static int findMaximum(int[] arr, int n) {
        int low = 0;
        int high = n-1;

        while(low <= high) {
            int mid = (low + high) /2;

            if((mid == 0 || arr[mid] > arr[mid-1]) && (mid == n-1 || arr[mid] > arr[mid+1])) {
                return arr[mid];
            }
            else if(mid > 0 && arr[mid] < arr[mid-1]) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return -1; 
    }
    public static void main(String[] args) {
	int arr[] = {1,15,25,45,42,21,17,12,11};
	int n = 9;
	int ans = findMaximum(arr,n);
	System.out.println(ans);
    }
}
