// Element with left side smaller and right side greater
class Compute {
    static int findElement(int arr[], int n){

        for(int i=1;i<n-1;i++){
            int j=0;
            
            while(j<i&&arr[i]>=arr[j]){
                j++;
            }
            
            if(j==i){
                j++;
                while(j<n&&arr[i]<=arr[j]){
                    j++;
                }
            }
            if(j==n){
                return arr[i];
            }
        }
        return -1;
   }
   public static void main(String[] args) {
	int n = 4;
	int arr[] = {4,2,5,7};
	int ans = findElement(arr,n);

	System.out.println(ans);
   }
}
