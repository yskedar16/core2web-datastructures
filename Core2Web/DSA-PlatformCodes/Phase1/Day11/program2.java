//  2022. Convert 1D Array Into 2D Array

class Solution {
   static int[][] construct2DArray(int[] original, int m, int n) {
        if(original.length != m*n) {
            return new int[0][0];
        }
        int[][] arr = new int[m][n];

        int k = 0;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                arr[i][j] = original[k];
                k++;
            }
        }
        return arr;
    }
    public static void main(String[] args) {
	int original[] = {1,2,3,4};
       	int m = 2;
       	int n = 2;
	
	int ans[][] = construct2DArray(original,m,n);
	for(int i=0;i<ans.length;i++) {
		for(int j=0;j<ans[i].length;j++) {
			System.out.print(ans[i][j] + " ");
		}
		System.out.println();
	}
		
    }
}
