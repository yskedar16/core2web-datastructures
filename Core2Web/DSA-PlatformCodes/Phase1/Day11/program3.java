// Array Subset of another array

import java.util.*;

class Compute {
    static String isSubset(long a1[], long a2[], long n, long m) {
        Arrays.sort(a1);
        Arrays.sort(a2);

        int i = 0, j = 0;

        while (i < n && j < m) {
            if (a1[i] < a2[j]) {
                i++;
            } else if (a1[i] == a2[j]) {
                i++;
                j++;
            } else {
                return "No";
            }
        }

        if(j == m) {
            return "Yes";
        }
        else{
            return "No";
        }
    }
    public static void main(String[] args) {
	long a1[] = {1, 2, 3, 4, 4, 5, 6};
	long a2[] = {1, 2, 4};
	int n = 6;
	int m = 3;
	String ans = isSubset(a1,a2,n,m);
	System.out.println(ans);
    }
}
