//  1480. Running Sum of 1d Array

class Solution {
    static int[] runningSum(int[] nums) {
        int arr[] = new int[nums.length];
        arr[0] = nums[0];
        for(int i=1;i<nums.length;i++) {
            arr[i] = arr[i-1] + nums[i];
        }
        return arr;
    }
    public static void main(String[] args) {
	int nums[] = {1,2,3,4};
	int ans[] = runningSum(nums);
	for(int x:ans) {
		System.out.print(x + " ");
	}
    }
}
