import java.util.ArrayList;

class Solution1 {
    static int[] plusOne(int[] digits) {
        ArrayList<Integer> arr = new ArrayList<>();

        int carry = 1; 

        for (int i = digits.length - 1; i >= 0; i--) {
            int sum = digits[i] + carry;
            arr.add(0, sum % 10); 
            carry = sum / 10; 
        }

        
        if (carry > 0) {
            arr.add(0, carry);
        }

        int[] finalArr = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            finalArr[i] = arr.get(i);
        }

        return finalArr;
    }
    public static void main(String[] args) {
	int digits[] = {4,3,2,1};
	int ans[] = plusOne(digits);
	for(int i=0;i<ans.length;i++) {
		System.out.print(ans[i] + " ");
	}
    }
}

