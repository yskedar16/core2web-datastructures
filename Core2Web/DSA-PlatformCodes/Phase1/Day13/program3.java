// Minimum distance between two numbers
class Solution3 {
    static int minDist(int a[], int n, int x, int y) {
        int firstNo = -1; 
        int secondNo = -1; 
        int minDistance = Integer.MAX_VALUE;

        for (int i = 0; i < n; i++) {
            if (a[i] == x) {
                firstNo = i;
                if (secondNo != -1) {
                    minDistance = Math.min(minDistance, firstNo - secondNo);
                }
            } else if (a[i] == y) {
                secondNo = i;
                if (firstNo != -1) {
                    minDistance = Math.min(minDistance, secondNo - firstNo);
                }
            }
        }

        return (firstNo != -1 && secondNo != -1) ? minDistance : -1;
    }
    public static void main(String[] args) {
	int arr[] = {1,2,3,2};
	int x = 1;
	int y = 2;
	int ans = minDist(arr,4,x,y);
	System.out.println(ans);
    }
}

