class Solution3 {

	static int PairSum(int arr[] ,int sum) {
		int size = arr.length;
		int count = 0;
		
		for(int i=0;i<size;i++) {
			for(int j=i+1;j<size;j++) {
				if(arr[i] + arr[j] == sum) {
					count++;
				}
			}
		}
		return count;
	}
	public static void main(String[] args) {
		int arr[] = new int[]{1,5,7,1};
		int ans = PairSum(arr,6);

		System.out.println("Given array has " + ans + " count pairs");
	}

}
