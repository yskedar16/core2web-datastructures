// Majority Element

import java.util.*;

class FindingMajorityElement {

	int majorityEle(int arr[]) { 
		int n = arr.length;
		for(int i=0;i<n;i++) {
			int count = 0;
			for(int j=0;j<n;j++) {
				if(arr[j] == arr[i]) {
					count = count + 1;
				}
			}
			if(count > n/2) {
				return arr[i];
			}

		
		}
		return -1;

	}
}

class Solution {
	public static void main(String[] args) {
		FindingMajorityElement me = new FindingMajorityElement();
		int arr[] = new int[]{2,2,1,1,1,2,2};
		int ans = me.majorityEle(arr);

		if(ans != -1) {
			System.out.println("Majority Element is " + ans);
		}
		else {
			System.out.println("Array Does not have any majority Element");
		}
	}
}
