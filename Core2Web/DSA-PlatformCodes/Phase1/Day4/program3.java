class EvenAndOdd {
	static void reArrange(int[] arr, int N) {
        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();
        
        for(int i = 0; i < N; i++) {
            if(arr[i] % 2 == 0) {
                s1.push(arr[i]);
            } else {
                s2.push(arr[i]);
            }
        }
        
        for(int i = 0; i < N; i++) {
            if(i % 2 == 0) {
                arr[i] = s1.pop();
            } else {
                arr[i] = s2.pop();
            }
        }
    

}
