class NextGreaterElement {
	static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] arr = new int[nums1.length];

        for (int i = 0; i < nums1.length; i++) {
            boolean flag = false;

            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    for (int k = j + 1; k < nums2.length; k++) {
                        if (nums2[k] > nums2[j]) {
                            arr[i] = nums2[k];
                            flag = true;
                            break;
                        }
                    }
                    break;
                }
            }

            if (flag == false) {
                arr[i] = -1;
            }
        }

        return arr;
    }
    public static void main(String[] args) {
	int[] nums1 = {4,1,2};
	int[] nums2 = {1,3,4,2};
	int arr[] = nextGreaterElement(nums1,nums2);
	for(int i=0;i<arr.length;i++) {
		System.out.println(arr[i]);
	}
    }

   
}
