class MaxConsecutiveOnes {
    static int findMaxConsecutiveOnes(int[] nums) {
        int maxCount = 0;
        int count = 0;
        for(int i=0;i<nums.length;i++) {
            if(nums[i] == 0) {
                count = 0;
            }
            if(nums[i] == 1) {
                count++;
            }
            maxCount = Math.max(maxCount,count);
        }
        return maxCount;
    }

	
	public static void main(String[] args) {
		int nums[] = {1,1,0,1,1,1};
		int ans = findMaxConsecutiveOnes(nums);
		System.out.println(ans);
	       	
	}
}
