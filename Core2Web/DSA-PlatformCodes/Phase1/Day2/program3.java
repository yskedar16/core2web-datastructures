/*Code3 : Find the smallest and second smallest element in
an array
Company: Amazon, Goldman Sachs
Platform: GFG
Description:
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.

Example 2:
Input :
6
1 2 1 3 6 7
Output :
1 2
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1<=N<=105
1<=A[i]<=105*/


import java.io.*; 

class SecondSmallestNumber { 
	
	static void printSmallest(int arr[]) 
	{ 
		int first, second, arr_size = arr.length; 

		
		if (arr_size < 2) { 
			System.out.println(" Invalid Input "); 
			return; 
		} 

		first = second = Integer.MAX_VALUE; 
		for (int i = 0; i < arr_size; i++) { 	

			if (arr[i] < first) { 
				second = first; 
				first = arr[i];
			}
			else if (arr[i] < second && arr[i] != first) 
				second = arr[i]; 
		} 
		if (second == Integer.MAX_VALUE) 
			System.out.println("There is no second"
							+ "smallest element"); 
		else
			System.out.println("The smallest element is "
							+ first 
							+ " and second Smallest"
							+ " element is " + second); 
	} 

	
	public static void main(String[] args) 
	{ 
		int arr[] = { 12, 13, 1, 10, 34, 1 }; 
		printSmallest(arr); 
	} 
} 

