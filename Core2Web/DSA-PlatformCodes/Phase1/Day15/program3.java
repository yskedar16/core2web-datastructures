//Transpose of Matrix

class Solution
{
    static void transpose(int n,int a[][])
    {
       int[][] result = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[j][i] = a[i][j];
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = result[i][j];
            }
        }

    }
    
}
class Matrix {
    public static void main(String[] args) {
        Solution solution = new Solution();

        
        int n = 3;
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
	solution.transpose(n, matrix);
    }
}

