// 118. Pascal's Triangle
//
import java.util.*;
class Solution {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> Pascaltriangle = new ArrayList<>();

        for (int i = 0; i < numRows; i++) {
            List<Integer> list = new ArrayList<>();

            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    list.add(1);
                } else {
                    
                    int value = Pascaltriangle.get(i - 1).get(j - 1) + Pascaltriangle.get(i - 1).get(j);
                    list.add(value);
                }
            }

            Pascaltriangle.add(list);
        }

        return Pascaltriangle;
    }
}
class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();

        
        int numRows = 5;
        List<List<Integer>> result = solution.generate(numRows);

       
        System.out.println("Pascal's Triangle for numRows = " + numRows + ":");
        for (List<Integer> row : result) {
            System.out.println(row);
        }

        
    }
}

