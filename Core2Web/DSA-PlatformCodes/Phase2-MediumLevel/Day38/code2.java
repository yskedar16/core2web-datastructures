// Jump Game2

class Solution2 {
    public int jump(int[] nums) {
        int totalJumps = 0;
        int destination = nums.length - 1;
        int coverage = 0;
        int lastJumpInx = 0;

        for (int i = 0; i < nums.length - 1; i++) {
            coverage = Math.max(coverage, i + nums[i]);

            if (i == lastJumpInx) {
                lastJumpInx = coverage;
                totalJumps++;

                if (coverage >= destination) {
                    return totalJumps;
                }
            }
        }
        return totalJumps;
    }

    public static void main(String[] args) {
        Solution2 solution = new Solution2();

        int[] nums1 = {2, 3, 1, 1, 4};
        System.out.println(solution.jump(nums1)); 

        int[] nums2 = {2, 3, 0, 1, 4};
        System.out.println(solution.jump(nums2)); 

        int[] nums3 = {3, 2, 1, 0, 4};
        System.out.println(solution.jump(nums3)); 
    }
}

