// Jump Game

class Solution1 {
    public boolean canJump(int[] nums) {
       int finalPosition = nums.length - 1;

       for(int i = nums.length - 2; i >= 0; i--) {
           if(i + nums[i] >= finalPosition) {
               finalPosition = i;
           }
       }
       return finalPosition == 0;
    }

    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        int[] nums1 = {2, 3, 1, 1, 4};
        System.out.println(solution.canJump(nums1)); 

        int[] nums2 = {3, 2, 1, 0, 4};
        System.out.println(solution.canJump(nums2)); 
    }
}

