// Maximise Score

import java.util.Scanner;
class Solution1 {
    public static int maximumScore(int n, int m, int[] a, int[] b) {
        long sumA = 0, sumB = 0;
        int i = 0, j = 0;
        long mod = 1000000007;

        while (i < n && j < m) {
            if (a[i] < b[j]) {
                sumA += a[i++];
            } else if (a[i] > b[j]) {
                sumB += b[j++];
            } else {
                long maxSum = Math.max(sumA, sumB) + a[i];
                sumA = sumB = maxSum;
                i++;
                j++;
            }
        }

        while (i < n) {
            sumA += a[i++];
        }

        while (j < m) {
            sumB += b[j++];
        }

        return (int) (Math.max(sumA, sumB) % mod);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt(); 

        for (int testCase = 0; testCase < t; testCase++) {
            int n = scanner.nextInt(); 
            int m = scanner.nextInt(); 

            int[] a = new int[n];
            int[] b = new int[m];

            for (int i = 0; i < n; i++) {
                a[i] = scanner.nextInt();
            }

            for (int i = 0; i < m; i++) {
                b[i] = scanner.nextInt();
            }

            int result = maximumScore(n, m, a, b);
            System.out.println(result);
        }

        scanner.close();
    }
}

