// Next Permutation

class Solution1 {
    public void nextPermutation(int[] nums) {
        int idx = -1;
        
        for (int i = nums.length - 2; i >= 0; i--) {
            if (nums[i] < nums[i + 1]) {
                idx = i;
                break;
            }
        }

        
        if (idx == -1) {
            reverse(nums, 0, nums.length - 1);
            return;
        }

        
        for (int m = nums.length - 1; m > idx; m--) {
            if (nums[m] > nums[idx]) {
                int temp = nums[m];
                nums[m] = nums[idx];
                nums[idx] = temp;
                break;
            }
        }

       
        int k = nums.length - 1;
        for (int j = idx + 1; j <= (nums.length + idx) / 2; j++) {
            int temp = nums[k];
            nums[k] = nums[j];
            nums[j] = temp;
            k--;
        }
    }

    
    private void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }

    
    public static void main(String[] args) {
        Solution1 solution = new Solution1();
        int[] nums = {1, 2, 3};
        System.out.println("Original array: " + java.util.Arrays.toString(nums));
        solution.nextPermutation(nums);
        System.out.println("Next permutation: " + java.util.Arrays.toString(nums));
    }
}

