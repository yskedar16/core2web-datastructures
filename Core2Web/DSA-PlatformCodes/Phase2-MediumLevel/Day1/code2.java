// Kth Smallest Element
import java.util.*;
class Solution{
    static int kthSmallest(int[] arr, int l, int r, int k) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>((a, b) -> b - a);

        for (int i = l; i <= r; i++) {
            maxHeap.offer(arr[i]);
            if (maxHeap.size() > k) {
                maxHeap.poll();
            }
        }

        return maxHeap.peek();
    }
    public static void main(String[] args) {
	int arr[] = {7,10,4,3,20,15};
	int l = 0;
	int r = 5;
	int k = 3;

	int ans = kthSmallest(arr,l,r,k);
	System.out.println(ans);
    }
}
