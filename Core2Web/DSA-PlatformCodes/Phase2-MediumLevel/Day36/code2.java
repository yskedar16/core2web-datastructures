// Trapping Rain Water

class Solution2{

    static long trappingWater(int arr[], int n) {

        long cnt = 0;
        long ml = 0, mr = 0;
        int l=0, r=n-1;
        while(l<=r){
            if(arr[l]<arr[r]){
                if(arr[l]<ml){
                    cnt +=ml-arr[l];
                }
                else{
                    ml = arr[l];
                }
                l++;
            }
            else{
                if(arr[r]<mr){
                    cnt += mr-arr[r];
                }
                else{
                    mr = arr[r];
                }
                r--;
            }
        }
        return cnt;
    }
}
