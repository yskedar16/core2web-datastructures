import java.util.Arrays;

class Solution1 {
    public int threeSumClosest(int[] nums, int target) {
        int ans = nums[0] + nums[1] + nums[2];

        Arrays.sort(nums);

        for(int i = 0; i + 2 < nums.length; ++i) {
            if(i > 0 && nums[i] == nums[i - 1]){
                continue;
            }

            int l = i + 1;
            int r = nums.length - 1;

            while(l < r) {
                final int sum = nums[i] + nums[l] + nums[r];

                if(sum == target) {
                    return sum;
                }

                if(Math.abs(sum - target) < Math.abs(ans - target)) {
                    ans = sum;
                }
                if(sum < target) {
                    ++l;
                }
                else {
                    --r;
                }
            }
        }
        return ans;
    }
}

class Main1 {
    public static void main(String[] args) {
        Solution1 solution = new Solution1();
        
        int[] nums = { -1, 2, 1, -4 };
        int target = 1; 
        
        int result = solution.threeSumClosest(nums, target);
        System.out.println("Closest sum to target: " + result);
    }
}

