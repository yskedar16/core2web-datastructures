// 2643. Row With Maximum Ones
class Solution2 {
    static int[] rowAndMaximumOnes(int[][] mat) {
        int maxCount = 0;
        int rowIndex = -1;

        for (int i = 0; i < mat.length; i++) {
            int count = 0;
            for (int j = 0; j < mat[i].length; j++) {
                if (mat[i][j] == 1) {
                    count++;
                }
            }

            if (count > maxCount || (count == maxCount && rowIndex == -1) || (count == maxCount && i < rowIndex)) {
                maxCount = count;
                rowIndex = i;
            }
        }

        return new int[]{rowIndex, maxCount};
    }
    public static void main(String[] args) {
	int mat[][] = {{0,1},{1,0}};
	int ans[] = rowAndMaximumOnes(mat);
	for(int i=0;i<ans.length;i++) {
		System.out.print(ans[i]);
	}
    }
}
