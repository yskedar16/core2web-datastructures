// Subset
import java.util.*;

class Solution1 {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        backtrack(result, new ArrayList<>(), nums, 0);
        return result;
    }

    private void backtrack(List<List<Integer>> result, List<Integer> temp, int[] nums, int start) {
        result.add(new ArrayList<>(temp));
        for (int i = start; i < nums.length; i++) {
            temp.add(nums[i]);
            backtrack(result, temp, nums, i + 1);
            temp.remove(temp.size() - 1);
        }
    }

    public static void main(String[] args) {
        Solution1 solution = new Solution1();

   
        int[] nums1 = {1, 2, 3};
        List<List<Integer>> subsets1 = solution.subsets(nums1);
        System.out.println("Subsets of [1, 2, 3]: " + subsets1);

        
        int[] nums2 = {0};
        List<List<Integer>> subsets2 = solution.subsets(nums2);
        System.out.println("Subsets of [0]: " + subsets2);
    }
}

