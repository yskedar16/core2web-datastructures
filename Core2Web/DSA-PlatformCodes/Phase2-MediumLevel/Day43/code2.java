import java.util.ArrayList;
import java.util.List;

class Solution2 {
    private void solve(String digit, StringBuilder output, int index, List<String> ans, String[] mapping) {
        if (index >= digit.length()) {
            ans.add(output.toString());
            return;
        }
        
        int number = digit.charAt(index) - '0';
        String value = mapping[number];
        
        for (int i = 0; i < value.length(); i++) {
            output.append(value.charAt(i));
            solve(digit, output, index + 1, ans, mapping);
            output.deleteCharAt(output.length() - 1);
        }
    }
    
    public List<String> letterCombinations(String digits) {
        List<String> ans = new ArrayList<>();
        if (digits.length() == 0)
            return ans;
        
        StringBuilder output = new StringBuilder();
        int index = 0;
        String[] mapping = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        
        solve(digits, output, index, ans, mapping);
        return ans;
    }

    public static void main(String[] args) {
        Solution2 solution = new Solution2();

        String digits1 = "23";
        System.out.println("Letter combinations for digits1: " + solution.letterCombinations(digits1));

        String digits2 = "";
        System.out.println("Letter combinations for digits2: " + solution.letterCombinations(digits2));
    }
}

