class Solution1 {
    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        int[] cardPoints1 = {1, 2, 3, 4, 5, 6, 1};
        int k1 = 3;
        System.out.println("Max score for cardPoints1: " + solution.maxScore(cardPoints1, k1));

        int[] cardPoints2 = {2, 2, 2};
        int k2 = 2;
        System.out.println("Max score for cardPoints2: " + solution.maxScore(cardPoints2, k2));
    }

    public int maxScore(int[] cardPoints, int k) {
        int maxScore = 0;

        int totalSum = 0;
        for (int card : cardPoints) {
            totalSum += card;
        }

        int windowSize = cardPoints.length - k;
        int sum = 0;
        for (int i = 0; i < windowSize; i++) {
            sum += cardPoints[i];
        }
        maxScore = sum;

        for (int i = windowSize; i < cardPoints.length; i++) {
            sum += cardPoints[i] - cardPoints[i - windowSize];
            maxScore = Math.min(maxScore, sum);
        }

        return totalSum - maxScore;
    }
}

