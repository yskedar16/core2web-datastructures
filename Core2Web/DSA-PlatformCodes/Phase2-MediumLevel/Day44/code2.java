import java.util.*;

class Solution2 {
    public static void main(String[] args) {
        
        int[] arr = {1, 4, 2, 3, 5};
        int s = 5;
        System.out.println("Number of subarrays with sum " + s + ": " + findAllSubarraysWithGivenSum(arr, s));
    }

    public static int findAllSubarraysWithGivenSum(int[] arr, int s) {
        int n = arr.length; 
        Map<Integer, Integer> mpp = new HashMap<>();
        int preSum = 0, cnt = 0;
        mpp.put(0, 1); 
        for (int i = 0; i < n; i++) {
            preSum += arr[i];
            int remove = preSum - s;
            cnt += mpp.getOrDefault(remove, 0);
            mpp.put(preSum, mpp.getOrDefault(preSum, 0) + 1);
        }
        return cnt;
    }
}

