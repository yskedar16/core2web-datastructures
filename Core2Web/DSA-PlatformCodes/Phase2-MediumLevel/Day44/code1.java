import java.util.*;

class Solution1 {
    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        String[] transactions = {"alice,20,800,mtv", "alice,50,100,beijing"};
        List<String> invalidTransactions = solution.invalidTransactions(transactions);
        System.out.println("Invalid Transactions:");
        for (String invalidTransaction : invalidTransactions) {
            System.out.println(invalidTransaction);
        }
    }

    public List<String> invalidTransactions(String[] transactions) {
        Map<String, List<Transaction>> userTransactions = new HashMap<>();
        List<String> result = new ArrayList<>();

        for (String t : transactions) {
            String[] parts = t.split(",");
            String name = parts[0];
            int time = Integer.parseInt(parts[1]);
            int amount = Integer.parseInt(parts[2]);
            String city = parts[3];

            if (!userTransactions.containsKey(name)) {
                userTransactions.put(name, new ArrayList<>());
            }
            userTransactions.get(name).add(new Transaction(name, time, amount, city));
        }

        for (String name : userTransactions.keySet()) {
            List<Transaction> transactionsList = userTransactions.get(name);
            for (int i = 0; i < transactionsList.size(); i++) {
                Transaction current = transactionsList.get(i);
                if (current.amount > 1000) {
                    result.add(current.toString());
                } else {
                    for (int j = 0; j < transactionsList.size(); j++) {
                        if (i != j) {
                            Transaction other = transactionsList.get(j);
                            if (!current.city.equals(other.city) && Math.abs(current.time - other.time) <= 60) {
                                result.add(current.toString());
                                break;
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    private class Transaction {
        String name;
        int time;
        int amount;
        String city;

        public Transaction(String name, int time, int amount, String city) {
            this.name = name;
            this.time = time;
            this.amount = amount;
            this.city = city;
        }

        @Override
        public String toString() {
            return name + "," + time + "," + amount + "," + city;
        }
    }
}

