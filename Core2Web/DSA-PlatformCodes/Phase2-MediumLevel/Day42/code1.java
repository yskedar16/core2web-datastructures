class Solution1 {
    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        int[] nums1 = {2, 3, 1, 1, 4};
        System.out.println("Minimum number of jumps for nums1: " + solution.jump(nums1));

        int[] nums2 = {2, 3, 0, 1, 4};
        System.out.println("Minimum number of jumps for nums2: " + solution.jump(nums2));
    }

    public int jump(int[] nums) {
        int totalJumps = 0;
        int destination = nums.length - 1;
        int coverage = 0;
        int lastJumpInx = 0;

        for (int i = 0; i < nums.length - 1; i++) {
            coverage = Math.max(coverage, i + nums[i]);

            if (i == lastJumpInx) {
                lastJumpInx = coverage;
                totalJumps++;

                if (coverage >= destination) {
                    return totalJumps;
                }
            }
        }
        return totalJumps;
    }
}

