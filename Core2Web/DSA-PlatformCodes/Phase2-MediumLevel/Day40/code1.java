// Maximum area of a piece of cake After Horizontal and Vertical Cuts

import java.util.Arrays;

class Solution1 {
    public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        int maxHorizontalGap = getMaxGap(h, horizontalCuts);
        int maxVerticalGap = getMaxGap(w, verticalCuts);

        long maxArea = (long) maxHorizontalGap * maxVerticalGap;
        return (int) (maxArea % 1000000007);
    }

    private int getMaxGap(int length, int[] cuts) {
        Arrays.sort(cuts);
        int maxGap = cuts[0];

        for (int i = 1; i < cuts.length; i++) {
            maxGap = Math.max(maxGap, cuts[i] - cuts[i - 1]);
        }

        return Math.max(maxGap, length - cuts[cuts.length - 1]);
    }

    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        int h = 5;
        int w = 4;
        int[] horizontalCuts = {1, 2, 4};
        int[] verticalCuts = {1, 3};

        int maxArea = solution.maxArea(h, w, horizontalCuts, verticalCuts);
        System.out.println("Maximum Area: " + maxArea);
    }
}

