//K-diff Pairs in an Array
//
import java.util.Arrays;

class Solution1 {
    static int findPairs(int[] nums, int k) {
        Arrays.sort(nums);

        int pairCount = 0;

        for(int i = 0; i < nums.length - 1; i++) {
            if(i > 0 && nums[i] == nums[i - 1])
                continue;

            for(int j = i + 1; j < nums.length; j++) {
                if(Math.abs(nums[j] - nums[i]) == k) {
                    pairCount++;
                    break; 
                } else if(Math.abs(nums[j] - nums[i]) > k) {
                    break; 
                }
            }
        }
        return pairCount;
    }
    public static void main(String[] args) {
	int nums[] = new int[]{3,1,4,1,5};
	int k = 2;
	int ans = findPairs(nums,k);
	System.out.println(ans);
    }
}

