import java.util.*;

class Solution2 {
    public int find(int arr[], int i, int dp[]) {
        if (i == arr.length - 1) return arr[i];
        if (i >= arr.length) return 0;
        if (dp[i] != -1) return dp[i];
        int pick = arr[i] + find(arr, i + 2, dp);
        int no = find(arr, i + 1, dp);
        return dp[i] = Math.max(pick, no);
    }

    public int FindMaxSum(int arr[], int n) {
        int dp[] = new int[n];
        for (int i = 0; i < n; i++) dp[i] = -1;
        int ans = find(arr, 0, dp);
        return ans;
    }

    public static void main(String[] args) {
        Solution2 solution = new Solution2();
        int[] arr = {3, 2, 7, 10};
        int n = arr.length;
        int maxSum = solution.FindMaxSum(arr, n);
        System.out.println("Maximum sum using alternate elements: " + maxSum);
    }
}

