// Longest Consecative Sequence

import java.util.*;

class Solution1 {
    public int longestConsecutive(int[] nums) {
        int maxSeq = 0;
        Set<Integer> sequences = new HashSet();
        for (int num : nums) {
            sequences.add(num);
        }
        for (int num : nums) {
            int nextInSeq = num + 1;
            int prevInSeq = num - 1;
            int currentSequence = 1;
            while (sequences.remove(prevInSeq--)) {
                currentSequence++;
            }
            while (sequences.remove(nextInSeq++)) {
                currentSequence++;
            }
            if (currentSequence > maxSeq) {
                maxSeq = currentSequence;
            }
        }
        return maxSeq;
    }

    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        int[] nums1 = {100, 4, 200, 1, 3, 2};
        System.out.println(solution.longestConsecutive(nums1)); 

        int[] nums2 = {0, 3, 7, 2, 5, 8, 4, 6, 0, 1};
        System.out.println(solution.longestConsecutive(nums2)); 
    }
}

