import java.util.ArrayList;
import java.util.Collections;

class Solution2 {
    public static ArrayList<Double> findMedian(ArrayList<Integer> arr, int n, int m) {
        ArrayList<Double> medians = new ArrayList<>();
        for (int i = 0; i <= n - m; i++) {
            ArrayList<Integer> subArray = new ArrayList<>(arr.subList(i, i + m));
            Collections.sort(subArray); 
            double median;
            if (m % 2 == 0) {
                median = (subArray.get(m / 2 - 1) + subArray.get(m / 2)) / 2.0;
            } else {
                median = subArray.get(m / 2);
            }
            medians.add(median);
        }
        return medians;
    }    
}

