// . Maximum Subarray

class Solution1 {
    static int maxSubArray(int[] nums) {

        int maximumSum = Integer.MIN_VALUE;
        int sum = 0;

        for (int i = 0; i < nums.length; i++) {

            sum = sum + nums[i];

            if (sum > maximumSum) {
                maximumSum = sum;
            }

            if (sum < 0) {
                sum = 0;
            }
        }

        return maximumSum;
    }
    public static void main(String[] args) {
	int nums[] = {-2,1,-3,4,-1,2,1,-5,4};
	int ans = maxSubArray(nums);
	System.out.println(ans);
    }
}
