// Find All Duplicates in an Array

import java.util.*;
class Solution1 {
    static List<Integer> findDuplicates(int[] nums) {
        List<Integer> finalList = new ArrayList<>();
        HashMap<Integer, Integer> hm = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            hm.put(num, hm.getOrDefault(num, 0) + 1);
            if (hm.get(num) == 2) {
                finalList.add(num);
            }
        }
        return finalList;
    }
    public static void main(String[] args) {
	int arr[] = {4,3,2,7,8,2,3,1};
	findDuplicates(arr);
    }
}
