// Find the Duplicate Number

import java.util.*;
class Solution2 {
    static int findDuplicate(int[] nums) {
        HashMap<Integer,Integer> hm = new HashMap<>();

        for(int i=0;i<nums.length;i++) {
            int num = nums[i];
            hm.put(num, hm.getOrDefault(num,0) + 1);
        }
        for (Map.Entry<Integer, Integer> entry : hm.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();

            if(value >= 2) {
                return key;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
	int arr[] = {1,3,4,2,2};

	int ans = findDuplicate(arr);
	System.out.println(ans);
    }
}
