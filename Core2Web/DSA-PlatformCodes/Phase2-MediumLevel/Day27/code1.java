// Maximum Product Subarray

class Solutioni1 {
    int maxProduct(int[] nums) {
        int maxProduct = Integer.MIN_VALUE;

        for (int i = 0; i < nums.length; i++) {
            int prod = 1;

            for (int j = i; j < nums.length; j++) {
                prod = prod * nums[j];
                maxProduct = Math.max(maxProduct, prod);
            }
        }
        return maxProduct;
    }
}

class Main {
    public static void main(String[] args) {
        Solutioni1 solution = new Solutioni1();

        int[] nums = {2, 3, -2, 4}; 

        int result = solution.maxProduct(nums);
        System.out.println("Maximum product of a subarray: " + result);
    }
}

