// Best Time To Buy and Sell Stock 2

class Solution1 {
    public int maxProfit(int[] prices) {
        int maxProfit = 0;

        for (int i = 1; i < prices.length; i++) {
            int profit = prices[i] - prices[i - 1];
            if (profit > 0) {
                maxProfit += profit;
            }
        }

        return maxProfit;
    }

    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        int[] prices = {7, 1, 5, 3, 6, 4};
        int maxProfit = solution.maxProfit(prices);
        System.out.println(maxProfit);

        int[] prices2 = {1, 2, 3, 4, 5};
        int maxProfit2 = solution.maxProfit(prices2);
        System.out.println(maxProfit2);

        int[] prices3 = {7, 6, 4, 3, 1};
        int maxProfit3 = solution.maxProfit(prices3);
        System.out.println(maxProfit3);
    }
}

