// Product of Array Except itself

class Solution1 {
    static int[] productExceptSelf(int[] nums) {
        int ans[] = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            ans[i] = findProduct(nums, i);
        }
        return ans;
    }

    static int findProduct(int[] nums, int key) {
        int product = 1;
        for (int i = 0; i < nums.length; i++) {
            if (i == key) {
                continue;
            } else {
                product = product * nums[i];
            }
        }
        return product;
    }

    public static void main(String[] args) {
	int arr[] = new int[]{1,2,3,4};
	int ans[] = productExceptSelf(arr);
	for(int i=0;i<ans.length;i++) {
		System.out.print(ans[i] + " ");
	}
    }
}

