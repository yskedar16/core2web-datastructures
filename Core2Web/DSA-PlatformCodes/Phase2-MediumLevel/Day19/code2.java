// 
class Solution2 {
    static int zeroFilledSubarray(int[] nums) {
        int count = 0;
        int result = 0;

        for (int num : nums) {
            if (num == 0) {
                count++;
            } else {
                result += count * (count + 1) / 2; 
                count = 0;
            }
        }

        result += count * (count + 1) / 2;

        return result;
    }
    public static void main(String[] args) {
	int nums[] = {1,3,0,0,2,0,0,4};
	int ans = zeroFilledSubarray(nums);
	System.out.println(ans);
    }
}
