// Subarray Sum Equals K

class Solution1 {
    static int subarraySum(int[] nums, int k) {
        int subarrayCount = 0;
        for(int i=0;i<nums.length;i++) {
            for(int j=i;j<nums.length;j++) {
                int sum = 0;
                for(int a=i;a<=j;a++) {
                    sum = sum + nums[a];
                }

                if(sum == k) {
                    subarrayCount++;
                }
            }
        }
        return subarrayCount;
    }
    public static void main(String[] args) {
	int arr[] = {1,1,1};
	int k = 2;
	int ans = subarraySum(arr,k);
	System.out.println(ans);
    }
}
