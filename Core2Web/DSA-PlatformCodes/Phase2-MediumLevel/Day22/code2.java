// Maximum of all subarrays of size k

import java.util.*;
class Solution2 {
    static ArrayList<Integer> max_of_subarrays(int arr[], int n, int k) {
        ArrayList<Integer> arrlist = new ArrayList<>();
        for (int i = 0; i <= n - k; i++) {
            int max = Integer.MIN_VALUE;
            for (int j = i; j < i + k; j++) {
                if (arr[j] > max) {
                    max = arr[j];
                }
            }
            arrlist.add(max);
        }
        return arrlist;
    }
    public static void main(String[] args) {
	int arr[] = {1,2,3,1,4,5,2,3,6};
	int k = 3;
	int n = 9;

	max_of_subarrays(arr,n,k);
    }
}
