// Kth Element of two Arrays

import java.util.Arrays;
class Solution2 {
    public long kthElement(int arr1[], int arr2[], int n, int m, int k) {
        int[] arr = new int[m + n];

        int i = 0;
        int j = 0;
        int x = 0;

        while (i < n) {
            arr[x++] = arr1[i++];
        }

        while (j < m) {
            arr[x++] = arr2[j++];
        }

        Arrays.sort(arr);

        return arr[k - 1];
    }
}

class Main2 {
    public static void main(String[] args) {
        Solution2 solution = new Solution2();

        int[] arr1 = {1, 3, 5, 7};
        int[] arr2 = {2, 4, 6, 8};
        int n = arr1.length;
        int m = arr2.length;
	int k = 5;

	long result = solution.kthElement(arr1, arr2, n, m, k);
        System.out.println("The k-th element is: " + result);
    }
}

