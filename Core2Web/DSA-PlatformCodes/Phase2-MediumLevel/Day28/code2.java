// Missing and Repeating Elements

import java.util.Arrays;

class Solution2 {
    public static void main(String[] args) {
        Solution2 solution = new Solution2();

        int[][] grid = {
            {1, 2, 3},
            {4, 2, 6},
            {7, 8, 9}
        };

        int[] result = solution.findMissingAndRepeatedValues(grid);

        System.out.println("Repeated value: " + result[0]);
        System.out.println("Missing value: " + result[1]);
    }

    public int[] findMissingAndRepeatedValues(int[][] grid) {
        int n = grid.length;
        int[] count = new int[n * n + 1];

        int repeated = -1, missing = -1;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                count[grid[i][j]]++;
            }
        }

        for (int i = 1; i <= n * n; i++) {
            if (count[i] == 2) {
                repeated = i;
            }
            if (count[i] == 0) {
                missing = i;
            }
        }

        return new int[]{repeated, missing};
    }
}

