// Subarray Sums Divisible By K
//
import java.util.HashMap;

class Solution1 {
    public int subarraysDivByK(int[] nums, int k) {
        int sum = 0;
        int ans = 0;
        HashMap<Integer, Integer> hm = new HashMap<>();
        hm.put(sum, 1);
        int rem = 0;

        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            rem = sum % k;
            if (rem < 0) {
                rem += k;
            }
            if (hm.containsKey(rem)) {
                ans += hm.get(rem);
                hm.put(rem, hm.get(rem) + 1);
            } else {
                hm.put(rem, 1);
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        Solution1 solution = new Solution1();

        int[] nums = {4, 5, 0, -2, -3, 1};
        int k = 5;

        int result = solution.subarraysDivByK(nums, k);
        System.out.println("Number of subarrays divisible by " + k + ": " + result);
    }
}

