// 
// Count the Subarrays Having product less than K
//
class Solution2 {
    public long countSubArrayProductLessThanK(long a[], int n, long k) {
        if (k <= 1) return 0; 
        
        int left = 0;
        long prod = 1;
        long count = 0;
        
        for (int right = 0; right < n; right++) {
            prod *= a[right];
            
            while (prod >= k) { 
                prod /= a[left];
                left++;
            }
            
            count += right - left + 1; 
        }
        
        return count;
    }

    public static void main(String[] args) {
        Solution2 solution = new Solution2();

        long[] a = {10, 5, 2, 6};
        int n = a.length;
        long k = 100;

        long result = solution.countSubArrayProductLessThanK(a, n, k);
        System.out.println("Number of subarrays with product less than " + k + ": " + result);
    }
}

