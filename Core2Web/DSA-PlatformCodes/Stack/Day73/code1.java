// Baseball Game

import java.util.*;

class Solution1 {
    public static void main(String[] args) {
        Solution1 solution = new Solution1();
        String[] operations = {"5", "2", "C", "D", "+"};
        System.out.println(solution.calPoints(operations));
    }

    public int calPoints(String[] operations) {
        Stack<Integer> stack = new Stack<>();

        for (String s : operations) {
            if (s.equals("+")) {
                stack.push(stack.peek() + stack.elementAt(stack.size()-2));
            }
            else if (s.equals("D")) {
                stack.push(stack.peek() * 2);
            }
            else if (s.equals("C")) {
                stack.pop();
            }
            else {
                stack.push(Integer.parseInt(s));
            }
        }

        int sum = 0;

        for (int i : stack) {
            sum += i;
        }

        return sum;
    }
}

