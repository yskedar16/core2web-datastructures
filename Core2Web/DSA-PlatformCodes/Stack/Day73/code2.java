// Implement Stack using LinkedLidst

import java.util.*;

class Solution2 {
    public static void main(String[] args) {
        twoStacks stacks = new twoStacks();
        stacks.push1(5);
        stacks.push2(10);
        System.out.println("Popped element from stack1: " + stacks.pop1());
        System.out.println("Popped element from stack2: " + stacks.pop2());
    }

    static class twoStacks {
        ArrayList<Integer> al;

        twoStacks() {
            al = new ArrayList<>();
            al.add(-1);
        }

        void push1(int x) {
            al.add(0, x);
        }

        void push2(int x) {
            al.add(x);
        }

        int pop1() {
            if (al.get(0) != -1)
                return al.remove(0);
            else
                return -1;
        }

        int pop2() {
            if (al.get(al.size() - 1) != -1)
                return al.remove(al.size() - 1);
            else
                return -1;
        }
    }
}

