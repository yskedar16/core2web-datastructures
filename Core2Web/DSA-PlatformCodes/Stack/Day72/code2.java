//Delete middle element of a stack

import java.util.*;

class Solution2 {
    public static void main(String[] args) {
        Solution solution = new Solution();
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        solution.deleteMid(stack, stack.size());
        System.out.println("Stack after deleting middle element: " + stack);
    }

    static class Solution {
        
        public void deleteMid(Stack<Integer> s, int sizeOfStack) {
            if (s.size() == (sizeOfStack + 1) / 2) {
                s.pop();
                return;
            }

            int top = s.pop();
            deleteMid(s, sizeOfStack);

            s.push(top);
        }
    }
}

