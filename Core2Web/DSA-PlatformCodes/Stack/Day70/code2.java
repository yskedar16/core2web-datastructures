class Solution2 {
    public static void main(String[] args) {
        Solution2 solution = new Solution2();
        int[] arr = {4, 2, 1, 5, 3};
        int n = arr.length;
        solution.immediateSmaller(arr, n);
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }

    void immediateSmaller(int arr[], int n) {
        for(int i = 0; i < n-1; i++) {
            if(arr[i] > arr[i+1]) {
                arr[i] = arr[i+1];
            } else {
                arr[i] = -1;
            }
        }
        arr[n-1] = -1;
        return;
    }
}

