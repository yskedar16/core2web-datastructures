//Flattening Tree -gfg 

class Node {
    int data;
    Node left, right;

    Node(int item) {
        data = item;
        left = right = null;
    }
}

class Solution2 {
    public static void flatten(Node root) {
        root = flattenTree(root);
    }

    static Node flattenTree(Node root) {
        if (root == null)
            return null;

        if (root.left != null) {
            Node temp = root.left;
            while (temp.right != null)
                temp = temp.right;
            temp.right = root.right;
            root.right = root.left;
            root.left = null;
        }
        
        root.right = flattenTree(root.right);
        return root;
    }

    public static void main(String[] args) {
        Node root = new Node(1);
        root.left = new Node(2);
        root.left.left = new Node(3);
        root.left.right = new Node(4);
        root.right = new Node(5);
        root.right.right = new Node(6);

        flatten(root);

        
        printFlattenedTree(root);
    }

    private static void printFlattenedTree(Node root) {
        while (root != null) {
            System.out.print(root.data + " ");
            root = root.right;
        }
        System.out.println();
    }
}

