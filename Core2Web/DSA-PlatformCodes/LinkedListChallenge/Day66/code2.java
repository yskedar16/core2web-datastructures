//Remove loop from linkedList

class Node {
    int data;
    Node next;
    Node(int data) {
        this.data = data;
        next = null;
    }
}

class Solution {
    public static void removeLoop(Node head){

        Node slow=head;
        Node fast=head;

        while(fast!=null && fast.next!=null && fast.next.next!=null)
        {
            slow=slow.next;
            fast=fast.next.next;

            if(slow==fast)
            {
                slow=head;

                while(slow!=fast)
                {
                    slow=slow.next;
                    fast=fast.next;
                }
                Node temp=slow;
                while(temp.next!=slow){
                    temp=temp.next;
                }
                temp.next=null;
            }
        }
    }
}

class Main2 {
    public static void main(String[] args) {
        
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);

        
        head.next.next.next.next.next = head.next;

        Solution.removeLoop(head);

        
        while (head != null) {
            System.out.print(head.data + " ");
            head = head.next;
        }
    }
}

