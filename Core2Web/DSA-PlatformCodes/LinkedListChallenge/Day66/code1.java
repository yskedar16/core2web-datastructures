// Remove nodes from LinkedList

import java.util.*;

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
    }
}

class Solution {
    public ListNode removeNodes(ListNode head) {
        ListNode cur = head;
        List<ListNode> list = new ArrayList<>();
        while (cur != null) {
            list.add(cur);
            cur = cur.next;
        }
        int max = 0;
        int[] arr = new int[list.size()];
        
        for (int i = list.size() - 1; i >= 0; i--) {
            if (max <= list.get(i).val) {
                arr[i] = 1;
                max = list.get(i).val;
            }
        }
        boolean headSet = false;
        cur = head;
        
        for (int i = 0; i < list.size(); i++) {
            if (arr[i] == 1) {
                if (!headSet) {
                    headSet = true;
                    head = list.get(i);
                    cur = head;
                } else {
                    cur.next = list.get(i);
                    cur = cur.next;
                }
            }
        }
        cur.next = null;
        return head;
    }
}

class Main1 {
    public static void main(String[] args) {
        
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);

        
        Solution solution = new Solution();

        
        ListNode result = solution.removeNodes(head);
        while (result != null) {
            System.out.print(result.val + " ");
            result = result.next;
        }
    }
}

