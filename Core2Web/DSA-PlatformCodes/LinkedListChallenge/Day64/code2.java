//LimkedList Cycle 2
import java.util.*;
class Solution2 {
    public ListNode detectCycle(ListNode head) {
         if(head== null || head.next == null) return null;

        ListNode fast= head;
        ListNode slow= head;
        ListNode first= head;

        while(fast!=  null && fast.next!= null){
             slow= slow.next;
             fast= fast.next.next;

            if(fast == slow){

              while(first!=  slow){
                   first= first.next;
                   slow= slow.next;
              }
              return slow;
             }
        }
        return null;
    }

    public static void main(String[] args) {
        
        ListNode head = new ListNode(3);
        head.next = new ListNode(2);
        head.next.next = new ListNode(0);
        head.next.next.next = new ListNode(-4);
        head.next.next.next.next = head.next;

        Solution2 solution = new Solution2();
        ListNode cycleStart = solution.detectCycle(head);

        if (cycleStart != null) {
            System.out.println("Cycle starts at node with value: " + cycleStart.val);
        } else {
            System.out.println("No cycle detected.");
        }
    }
}

