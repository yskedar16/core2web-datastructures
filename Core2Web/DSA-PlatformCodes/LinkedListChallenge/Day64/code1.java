class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}

class Solution1 {
    public ListNode rotateRight(ListNode head, int k) {
        if (head == null || k == 0) return head;
        
        int size = 1;
        ListNode node = head;
        while (node.next != null) {
            size++;
            node = node.next;
        }
        node.next = head;
        
        int stepsToNewHead = size - (k % size);
        for (int i = 0; i < stepsToNewHead; i++) {
            node = node.next;
        }
        
        ListNode newHead = node.next;
        node.next = null;
        
        return newHead;
    }

    public static void main(String[] args) {
        
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);

        Solution1 solution = new Solution1();
        ListNode rotatedHead = solution.rotateRight(head, 2);

        
        printList(rotatedHead);
    }

    private static void printList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
        System.out.println();
    }
}

