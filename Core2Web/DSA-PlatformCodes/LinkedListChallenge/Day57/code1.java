class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { 
        val = x; 
    }
}

class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode temp1 = list1;
        ListNode temp2 = list2;

        if(list1==null && list2==null) {
            return null;
        }

        ListNode ans = new ListNode(0);
        int first = 1;
        ListNode head = ans;

        while(temp1 != null && temp2 != null) {
            if(temp1.val > temp2.val) {
                ans.next = new ListNode(temp2.val);
                temp2 = temp2.next;
            }
            else {
                ans.next = new ListNode(temp1.val);
                temp1 = temp1.next;
            }
            ans = ans.next;
            if(first==1) {
                head = ans;
                first = 0;
            }
        }

        while(temp1 != null) {
            ans.next = new ListNode(temp1.val);
            ans = ans.next;
            temp1 = temp1.next;
            if(first==1) {
                head = ans;
                first = 0;
            }
        }

        while(temp2 != null) {
            ans.next = new ListNode(temp2.val);
            ans = ans.next;
            temp2 = temp2.next;
            if(first==1) {
                head = ans;
                first = 0;
            }
        }

        return head;
    }
}

class Main1 {
    public static void main(String[] args) {
        ListNode list1 = new ListNode(1);
        list1.next = new ListNode(2);
        list1.next.next = new ListNode(4);

        ListNode list2 = new ListNode(1);
        list2.next = new ListNode(3);
        list2.next.next = new ListNode(4);

        Solution solution = new Solution();
        ListNode mergedList = solution.mergeTwoLists(list1, list2);

        printList(mergedList);
    }

    static void printList(ListNode head) {
        while (head != null) {
            System.out.print(head.val);
            if (head.next != null) {
                System.out.print(" -> ");
            }
            head = head.next;
        }
        System.out.println();
    }
}

