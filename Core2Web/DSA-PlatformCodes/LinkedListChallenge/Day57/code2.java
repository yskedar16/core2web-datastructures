import java.util.PriorityQueue;

class Node {
    int data;
    Node next;

    Node(int data) {
        this.data = data;
        this.next = null;
    }
}

class Solution {
    Node compute(Node head) {
        Node temp = head;
        PriorityQueue<Integer> q = new PriorityQueue<>();
        while (temp != null) {
            if (q.size() > 0) {
                int d = q.peek();
                while (d < temp.data && q.size() > 0) {
                    q.poll();
                    if (q.size() > 0)
                        d = q.peek();
                }
            }
            q.add(temp.data);
            temp = temp.next;
        }
        head = null;
        while (q.size() > 0) {
            Node tempN = new Node(q.poll());
            if (head == null) {
                head = tempN;
            } else {
                tempN.next = head;
                head = tempN;
            }
        }
        return head;
    }
}

class Main2 {
    public static void main(String[] args) {
        Node head = new Node(3);
        head.next = new Node(1);
        head.next.next = new Node(7);
        head.next.next.next = new Node(5);
        head.next.next.next.next = new Node(4);

        Solution solution = new Solution();

        System.out.println("Original Linked List:");
        printLinkedList(head);

        head = solution.compute(head);

        System.out.println("\nLinked List after compute:");
        printLinkedList(head);
    }

    public static void printLinkedList(Node head) {
        Node temp = head;
        while (temp != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println();
    }
}

