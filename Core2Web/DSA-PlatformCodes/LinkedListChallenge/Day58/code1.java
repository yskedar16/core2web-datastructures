class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
    }
}

class Solution {
    public int getDecimalValue(ListNode head) {
        int n = 0;
        for (ListNode h = head; h != null; h = h.next) {
            n++;
        }

        int ans = 0;
        for (; head != null; head = head.next) {
            n--;
            if (head.val == 1) {
                ans += Math.pow(2, n);
            }
        }
        
        return ans;
    }
}

class Main1 {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(0);
        head.next.next = new ListNode(1);

        Solution solution = new Solution();
        int decimalValue = solution.getDecimalValue(head);
        System.out.println("Decimal value: " + decimalValue);
    }
}

