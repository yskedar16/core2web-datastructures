class Node {
    int data;
    Node next;

    Node(int d) {
        data = d;
        next = null;
    }
}

class Solution {
       static Node segregate(Node head) {
        int zero = 0;
        int one = 0;
        int two = 0;

        Node temp = head;
        while (temp != null) {
            if (temp.data == 0) {
                zero++;
            } else if (temp.data == 1) {
                one++;
            } else {
                two++;
            }
            temp = temp.next;
        }
        temp = head;
        while (temp != null) {
            if (zero > 0) {
                temp.data = 0;
                zero--;
            } else if (one > 0) {
                temp.data = 1;
                one--;
            } else {
                temp.data = 2;
                two--;
            }
            temp = temp.next;
        }

        return head;
    }
}

class Main2 {
    public static void main(String[] args) {
        
        Node head = new Node(1);
        head.next = new Node(0);
        head.next.next = new Node(2);
        head.next.next.next = new Node(1);
        head.next.next.next.next = new Node(0);

        System.out.println("Before segregation:");
        printList(head);

        head = Solution.segregate(head);

        System.out.println("After segregation:");
        printList(head);
    }

    
    public static void printList(Node head) {
        Node temp = head;
        while (temp != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println();
    }
}

