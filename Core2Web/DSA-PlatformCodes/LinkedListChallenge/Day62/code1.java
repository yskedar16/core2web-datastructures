import java.util.HashMap;

class Node {
    int val;
    Node next, random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}

class Solution1 {
    public Node copyRandomList(Node head) {
        if (head == null) return head;
        Node nhead = new Node(head.val);
        Node tail = nhead, temp = head.next;
        HashMap<Node, Node> map = new HashMap<>();
        map.put(head, nhead);
        while (temp != null) {
            Node x = new Node(temp.val);
            tail.next = x;
            tail = x;
            map.put(temp, x);
            temp = temp.next;
        }
        Node t = nhead;
        while (head != null) {
            if (head.random != null) {
                t.random = map.get(head.random);
            }
            t = t.next;
            head = head.next;
        }
        return nhead;
    }

    public static void main(String[] args) {
        Node head = new Node(7);
        head.next = new Node(13);
        head.next.next = new Node(11);
        head.next.next.next = new Node(10);
        head.next.next.next.next = new Node(1);

        head.random = null;
        head.next.random = head;
        head.next.next.random = head.next.next.next.next;
        head.next.next.next.random = head.next.next;
        head.next.next.next.next.random = head;

        Solution1 solution = new Solution1();
        Node copiedList = solution.copyRandomList(head);

        System.out.println("Original List with Random Pointers:");
        printListWithRandom(head);

        System.out.println("Copied List with Random Pointers:");
        printListWithRandom(copiedList);
    }

    private static void printListWithRandom(Node head) {
        while (head != null) {
            System.out.print("(" + head.val + " -> ");
            if (head.random != null)
                System.out.print(head.random.val);
            else
                System.out.print("null");
            System.out.print(") ");
            head = head.next;
        }
        System.out.println();
    }
}

