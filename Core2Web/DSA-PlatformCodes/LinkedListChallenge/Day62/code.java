// 116. Populating Next Right Pointers in Each Node

class Node {
    int val;
    Node left, right, next;

    public Node(int val) {
        this.val = val;
        this.left = this.right = this.next = null;
    }
}

class Solution2 {
    public Node connect(Node root) {
        if (root == null) return root;

        Node leftNode = root;
        while (leftNode.left != null) {
            Node head = leftNode;
            while (head != null) {
                head.left.next = head.right;
                if (head.next != null) {
                    head.right.next = head.next.left;
                }
                head = head.next;
            }
            leftNode = leftNode.left;
        }
        return root;
    }

    public static void main(String[] args) {
        
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.right.right = new Node(7);

        Solution2 solution = new Solution2();
        Node connectedRoot = solution.connect(root);

        
        printConnectedNodes(connectedRoot);
    }

    private static void printConnectedNodes(Node root) {
        while (root != null) {
            Node current = root;
            while (current != null) {
                System.out.print(current.val);
                if (current.next != null)
                    System.out.print(" -> ");
                current = current.next;
            }
            System.out.println();
            root = root.left;
        }
    }
}

