// Remove Nth Node From End of the List

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
        next = null;
    }
}

class Solution {
    int length(ListNode head) {
        int len = 0;
        ListNode temp = head;
        while (temp != null) {
            temp = temp.next;
            len++;
        }
        return len;
    }

    public ListNode removeNthFromEnd(ListNode head, int n) {
        int len = length(head);
        ListNode node = head;
        int pos = 1;
        while (pos < len - n) {
            node = node.next;
            pos++;
        }

        if (len == n)
            head = head.next;
        else if (node.next == null)
            return null;
        else
            node.next = node.next.next;

        return head;
    }
}

class Main2 {
    public static void main(String[] args) {
        
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);

        int n = 2; 

        Solution solution = new Solution();
        ListNode result = solution.removeNthFromEnd(head, n);

        
        while (result != null) {
            System.out.print(result.val + " ");
            result = result.next;
        }
    }
}

