// Delete Node in the linkedList

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
        next = null;
    }
}

class Solution {
    public void deleteNode(ListNode node) {
        ListNode temp;
        temp = node.next;
        node.val = temp.val;
        node.next = temp.next;
    }
}
class Main1 {
    public static void main(String[] args) {
        
        ListNode head = new ListNode(4);
        head.next = new ListNode(5);
        head.next.next = new ListNode(1);
        head.next.next.next = new ListNode(9);

        System.out.println("Before deletion:");
        printList(head);

                Solution solution = new Solution();
        solution.deleteNode(head.next);

        System.out.println("After deletion:");
        printList(head);
    }

    
    public static void printList(ListNode head) {
        ListNode temp = head;
        while (temp != null) {
            System.out.print(temp.val + " ");
            temp = temp.next;
        }
        System.out.println();
    }
}

