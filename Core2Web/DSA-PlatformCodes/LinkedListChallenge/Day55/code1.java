 class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }
class Solution {
    public boolean isPalindrome(ListNode head) {

        var fast = head;
        ListNode lastNode = null;

        while (fast != null && fast.next != null) {

            fast = fast.next.next;

            var nextHead = head.next;
            head.next = lastNode;
            lastNode = head;
            head = nextHead;
        }

        if (fast != null) head = head.next;

        while (head != null) {
            if (lastNode.val != head.val) {return false;}
            lastNode = lastNode.next;
            head = head.next;
        }

        return true;
    }
}

class Main1 {
    public static void main(String[] args) {
       
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(2);
        head.next.next.next.next = new ListNode(1);
        
        Solution solution = new Solution();
        boolean isPalindromic = solution.isPalindrome(head);
        
        System.out.println("Is the linked list palindrome? " + isPalindromic); 
    }
}

