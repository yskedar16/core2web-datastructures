 class Solution {
    public int findMaxValueOfEquation(int[][] points, int k) {
        int maximum = Integer.MIN_VALUE; 
        int res = 0;
        int mod = 0;
        int flag = 1;
        for(int i = 0;i<points.length-1;i++) {
            if(flag<i+1)
                flag = i+1;
            for(int j = flag;j<points.length;j++) {
                mod = points[i][0]-points[j][0]; 
                if(mod<0)
                    mod = -mod;
                if(mod>k)
                    break;
                res = points[i][1]+points[j][1]+mod; 
                if(maximum<res) {
                    maximum = res; 
                    flag = j-1;
                }
            }
        }
        return maximum;
    }
    
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        
        int[][] points = {{1,3},{2,0},{3,10},{4,5},{5,3}};
        int k = 2;
        int result = solution.findMaxValueOfEquation(points, k);
        System.out.println("Maximum value of the equation: " + result);
    }
}

